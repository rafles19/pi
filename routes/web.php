<?php

use App\Http\Controllers\backend\AboutController;
use App\Http\Controllers\backend\AgendaController;
use App\Http\Controllers\backend\CategoryController;
use App\Http\Controllers\backend\DashboardController;
use App\Http\Controllers\backend\ArticleController;
use App\Http\Controllers\backend\CatProductController;
use App\Http\Controllers\backend\FaqController;
use App\Http\Controllers\backend\InvoiceController;
use App\Http\Controllers\backend\LinkController;
use App\Http\Controllers\backend\ProductController;
use App\Http\Controllers\backend\ProductImageController;
use App\Http\Controllers\backend\SettingController;
use App\Http\Controllers\backend\SliderController;
use App\Http\Controllers\backend\StaticPageController;
use App\Http\Controllers\backend\TestimonialController;
use App\Http\Controllers\Role\UserController;
use App\Http\Controllers\frontend\CartController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Role\AdminController;
use App\Livewire\DynamicSelect;
use App\Livewire\ShowPosts;
use App\Models\About;
use App\Models\CatProduct;
use App\Models\Invoice;
use App\Models\Static_Page;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



Route::middleware(['auth', 'auth.admin'])->group(function () {
    Route::get('/dropdown', ShowPosts::class);
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('role.admin');
    Route::resource('/categories', CategoryController::class);
    Route::resource('/agenda', AgendaController::class);
    Route::resource('/article', ArticleController::class);
    Route::resource('/faq', FaqController::class);
    Route::resource('/invoice', InvoiceController::class);

    Route::resource('/category-product', CatProductController::class);
    Route::resource('/about', AboutController::class);
    Route::resource('/link', LinkController::class);
    Route::resource('/setting', SettingController::class);
    Route::resource('/slider', SliderController::class);
    Route::resource('/static-page', StaticPageController::class);
    // Pada routes/web.php

    // Route::resource('/account' , UserController::class);
    Route::resource('testimonial', TestimonialController::class);

    Route::group(['prefix' => 'laravel-filemanager'], function () {
        \UniSharp\LaravelFilemanager\Lfm::routes();
    });


    // Product Routes

    Route::get('/product', [ProductController::class, 'index'])->name('product');
    Route::get('/product/add', [ProductController::class, 'create'])->name('add_product');
    Route::post('/product/add', [ProductController::class, 'store'])->name('store_product');
    Route::get('/product/{product}/edit', [ProductController::class, 'edit'])->name('edit_product');
    Route::put('/product/{product}', [ProductController::class, 'update'])->name('update_product');
    Route::delete('/product/{id}', [ProductController::class, 'destroy'])->name('delete_product');
    Route::delete('/product/image/{id}', [ProductImageController::class, 'destroy'])->name('destroy');


    // Route::post('/add-to-cart', [CartController::class, 'addCart'])->name('add_to_cart');
    // Route::get('/cart',[CartController::class,'index'])->name('cart.index');
    // Route::post('/cart/store', [CartController::class, 'addToCart'])->name('cart.store');
    // Route::get('/shopping-cart', [HomeController::class, 'bookCart'])->name('shopping.cart');
    // Route::get('/product/{slug}/add', [HomeController::class, 'addProductoCart'])->name('addbook.to.cart');
    // Route::patch('/update-shopping-cart', [HomeController::class, 'updateCart'])->name('update.sopping.cart');
    // Route::delete('/delete-cart-product', [HomeController::class, 'deleteProduct'])->name('delete.cart.product');

});

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('/my-account', [UserController::class, 'index'])->name('user.index');
    Route::post('/', [CartController::class, 'store'])->name('cart.store');
    Route::post('/cart-details', [CartController::class, 'show'])->name('cart.show');
    Route::get('/cart', [CartController::class, 'index'])->name('cart');
    Route::post('/update-qty/{slug}', [CartController::class, 'updateQty'])->name('up.cart');
    // Route::delete('/drop',[CartController::class,'destroy'])->name('hapus.cart');
    // Route::post('/checkout/{slug}', 'CartController@checkout')->name('checkout');


    Route::delete('/{cartId}', [CartController::class, 'deleteCartItem'])->name('cart.delete');
    Route::get('/cart/items', [CartController::class, 'listAllCartItems'])->name('test');
    Route::delete('/cart/items', [CartController::class, 'deleteAllUserCartItems'])->name('cart.delete.all');
    Route::post('/checkout', [CartController::class, 'checkOut'])->name('checkout');


});

// Guest Homepage
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/', [ProductController::class, 'productHome'])->name('home');
Route::get('/produk/{slug}', [ProductController::class, 'productView'])->name('show');
