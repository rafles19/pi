<aside id="sidebar" class="sidebar">
  <ul class="sidebar-nav" id="sidebar-nav">
      <li class="nav-item">
          <a class="nav-link" href="{{ url('dashboard') }}">
              <i class="bi bi-grid"></i>
              <span>Dashboard</span>
          </a>
      </li><!-- End Dashboard Nav -->
      
      
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#components-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-menu-button-wide"></i><span>Article</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="components-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('article.index') }}">
              <i class="ri-article-fill"></i>
              <span>Article</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link collapsed " href="{{ route('categories.index') }}">
              <i class="ri-folder-2-line"></i>
              <span>Category</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('testimonial.index') }}">
              <i class="ri-message-3-line"></i>
              <span>Testimoni</span>
            </a>
          </li>
          
          

          
        </ul>
      </li><!-- End Components Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#components-navs" data-bs-toggle="collapse" href="#">
          <i class="bi bi-menu-button-wide"></i><span>Product</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="components-navs" class="nav-content collapse " data-bs-parent="#sidebar-navs">
          
          <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('product') }}">
              <i class="ri-link"></i>
              <span>Product</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('category-product.index') }}">
              <i class="ri-link"></i>
              <span>Category Product</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('invoice.index') }}">
              <i class="ri-link"></i>
              <span>Invoice</span>
            </a>
          </li>
          
        </ul>
      </li><!-- End Components Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#components-navweb" data-bs-toggle="collapse" href="#">
          <i class="bi bi-menu-button-wide"></i><span>Web Component</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="components-navweb" class="nav-content collapse " data-bs-parent="#sidebar-navweb">
          
          <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('about.index') }}">
              <i class="ri-folder-fill"></i>
              <span>About</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('setting.index') }}">
              <i class="ri-settings-5-line"></i>
              <span>Setting</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('static-page.index') }}">
              <i class="ri-page-separator"></i>
              <span>Static Page</span>
            </a>
          </li> 

          <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('faq.index') }}">
              <i class="ri-questionnaire-fill"></i>
              <span>Faq</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('slider.index') }}">
              <i class="ri-slideshow-2-line"></i>
              <span>Slider</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('link.index') }}">
              <i class="ri-link"></i>
              <span>Link</span>
            </a>
          </li>
          
        </ul>
      </li><!-- End Components Nav -->
  </ul>
</aside>
