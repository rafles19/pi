<!-- show.blade.php -->

@extends('backend.layout.template')

@section('title', 'Detail Artikel')
@section('content')
    {{-- Content --}}
    <main class="main" id="main">
        <div class="pagetitle">
            <h1>Testimoni</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item">Components</li>
                    <li class="breadcrumb-item"><a href="{{ route('testimonial.index') }}">Testimoni</a></li>
                    <li class="breadcrumb-item active">Detail</li>
                </ol>
            </nav>
        </div>

        <div class="card mb-3">
            <div class="card-header">
                <h5 class="card-title">{{ $testimonial->title }}</h5>
            </div>
            <div class="card-body">
                <h5 class="card-text">{{ $testimonial->impression }}</h5>
            </div>
            <div class="card-body">
                <p class="card-text">{!! $testimonial->description !!}</p>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Category: {{ $testimonial->category->nama }}</li>
                <li class="list-group-item">Status: {{ $testimonial->status == 0 ? 'Draft' : 'Published' }}</li>
                <li class="list-group-item">Published Date: {{ $testimonial->published_date }}</li>
            </ul>
            <div class="card-footer">
                <a href="{{ route('testimonial.index') }}" class="btn btn-primary">Back to List</a>
            </div>
        </div>
    </main>
@endsection

@push('js')
    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/vendor/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Template Main JS File -->
    <script src="{{ asset('assets/js/main.js') }}"></script>
@endpush
