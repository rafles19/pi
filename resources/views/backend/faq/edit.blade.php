@extends('backend.layout.template')
@section('content')
    <main id="main" class="main">
        <div class="pagenama">
            <h1>Edit Faq</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item">Components</li>
                    <li class="breadcrumb-item"><a href="{{ route('faq.index') }}">Faq</a></li>
                    </li>
                    <li class="breadcrumb-item active">Update</li>
                </ol>
            </nav>
        </div>

        <section class="section">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Please review this form</h5>

                            {{-- form --}}
                            <form class="row g-3" action="{{ route('faq.update', $faq->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')

                                <div class="col-md-6">
                                    <label for="section" class="form-label">Section</label>
                                    <div class="col-sm-30">
                                        <select name="section" id="section" class="form-control">
                                            <option value="Item" {{ $faq->section == 'Item' ? 'selected' : '' }}>Item</option>
                                            <option value="Description" {{ $faq->section == 'Description' ? 'selected' : '' }}>Description</option>
                                        </select>
                                        <div class="mt-3">
                                            @error('section')
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <i class="bi bi-exclamation-octagon me-1"></i>
                                                {{ $message }}
                                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                    aria-label="Close"></button>
                                            </div>
                                        @enderror
                                        </div>
                                        
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="sequence" class="form-label">Sequence</label>
                                    <div class="col-sm-30">
                                        <input type="number" name="sequence" id="sequence"
                                            class="form-control @error('sequence') is-invalid @enderror"
                                            value="{{ $faq->sequence }}">
                                            <div class="mt-3">
                                                @error('sequence')
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <i class="bi bi-exclamation-octagon me-1"></i>
                                                {{ $message }}
                                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                    aria-label="Close"></button>
                                            </div>
                                        @enderror
                                            </div>
                                        
                                    </div>                                                              
                                </div>

                                <div class="col-md-6">
                                    <label for="image" class="form-label">Image</label>
                                    <div class="col-sm-30">
                                        <input type="file" name="image" id="image" class="form-control">
                                        <div class="mt-3">
                                            @error('image')
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <i class="bi bi-exclamation-octagon me-1"></i>
                                                {{ $message }}
                                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                    aria-label="Close"></button>
                                            </div>
                                        @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <label for="title" class="form-label">Question</label>
                                    <div class="form-floating col-sm-30">
                                        <textarea name="title" class="form-control" placeholder="Leave a comment here" id="floatingTextarea" style="height: 100px;">{{ $faq->title }}</textarea>
                                        <div class="mt-3">
                                            @error('title')
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <i class="bi bi-exclamation-octagon me-1"></i>
                                        {{ $message }}
                                        <button type="button" class="btn-close" data-bs-dismiss="alert"
                                            aria-label="Close"></button>
                                    </div>
                                @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <label class="form-label" for="description">Description</label>
                                            <!-- TinyMCE Editor -->
                                            <textarea id="description" name="description" class="tinymce-editor">{{ $faq->description }}</textarea><!-- End TinyMCE Editor -->
                                            <div class="mt-3">
                                                @error('description')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror  
                                            </div>
                                </div>

                                

                                <div class="col-md-6">
                                    <label class="form-label" for="status">Status</label>
                                    <div class="col-sm-30">
                                        <select aria-label="Pilih Status" name="status" id="status" class="form-select">
                                        <option value="" hidden>Choose</option>
                                        <option value="1" {{ $faq->status == 1 ? 'selected' : '' }}>Published</option>
                                        <option value="0" {{ $faq->status == 0 ? 'selected' : '' }}>Draft</option>
                                        </select>

                                        <div class="mt-3">
                                            @error('status')
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <i class="bi bi-exclamation-octagon me-1"></i>
                                                {{ $message }}
                                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                    aria-label="Close"></button>
                                            </div>
                                        @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="float-end"> 
                                        <div class="mt-3">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Update Faq</button>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@push('js')
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>

    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/vendor/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/chart.js/chart.umd.js') }}"></script>
    <script src="{{ asset('assets/vendor/echarts/echarts.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/quill/quill.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/simple-datatables/simple-datatables.js') }}"></script>
    <script src="{{ asset('assets/vendor/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>

    <!-- Template Main JS File -->
    <script src="{{ asset('assets/js/main.js') }}"></script>

    <script src="https://cdn.ckeditor.com/4.17.1/standard/ckeditor.js"></script>

    <script>
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token=',
            clipboard_handleImages: false
        };

        document.addEventListener('DOMContentLoaded', function() {
            CKEDITOR.replace('myeditor', options);
        });
    </script>
@endpush
