@extends('backend.layout.template')

@section('content')
    <main id="main" class="main">
        <div class="pagenama">
            <h1>Edit Invoice</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item">Components</li>
                    <li class="breadcrumb-item"><a href="{{ route('invoice.index') }}">Invoice</a></li>
                    <li class="breadcrumb-item active">Update</li>
                </ol>
            </nav>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <section class="section">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Please review this form</h5>

                            {{-- form --}}
                            <form class="row g-3" action="{{ route('invoice.update', $invoice->id) }}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="col-md-6">
                                    <label for="user_id" class="form-label">Nama Pengguna</label>
                                    <div class="col-sm-30">
                                        <input type="text" name="user_id" id="user_id" class="form-control" value="{{ $invoice->user->name }}" disabled>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="product" class="form-label">Produk</label>
                                    <div class="col-sm-30">
                                        <input type="text" name="product_id" id="product_id" class="form-control" value="{{ $invoice->product->name }}" disabled>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="invoice_code" class="form-label">Kode Invoice</label>
                                    <div class="col-sm-30">
                                        <input type="text" name="invoice_code" id="invoice_code" class="form-control" value="{{ $invoice->invoice_code }}" disabled>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="quantity" class="form-label">Jumlah</label>
                                    <div class="col-sm-30">
                                        <input type="number" name="quantity" id="quantity" class="form-control" value="{{ $invoice->quantity }}" disabled>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="total" class="form-label">Total Pembayaran</label>
                                    <div class="col-sm-30">
                                        <input type="number" name="total" id="total" class="form-control" value="{{ $invoice->total }}" disabled>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="status_code" class="form-label">Status Pembayaran</label>
                                    <div class="col-sm-30">
                                        <select name="status_code" id="status_code" class="form-control">
                                            <option value="0" {{ $invoice->status_code == '0' ? 'selected' : '' }}>Pending</option>
                                            <option value="1" {{ $invoice->status_code == '1' ? 'selected' : '' }}>Paid</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="status_pengiriman" class="form-label">Status Pengiriman</label>
                                    <div class="col-sm-30">
                                        <select name="status_pengiriman" id="status_pengiriman" class="form-control">
                                            <option value="PENDING" {{ $invoice->status_pengiriman == 'PENDING' ? 'selected' : '' }}>Pending</option>
                                            <option value="SHIPPED" {{ $invoice->status_pengiriman == 'SHIPPED' ? 'selected' : '' }}>Shipped</option>
                                            <option value="DELIVERED" {{ $invoice->status_pengiriman == 'DELIVERED' ? 'selected' : '' }}>Delivered</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="no_resi" class="form-label">Nomor Resi</label>
                                    <div class="col-sm-30">
                                        <input type="text" name="no_resi" id="no_resi" class="form-control" value="{{ $invoice->no_resi }}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="kurir" class="form-label">Kurir</label>
                                    <div class="col-sm-30">
                                        <input type="text" name="kurir" id="kurir" class="form-control" value="{{ $invoice->kurir }}">
                                    </div>
                                </div>

                                

                                <div class="col-md-12">
                                    <div class="float-end"> 
                                        <div class="mt-3">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Update Invoice</button>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

