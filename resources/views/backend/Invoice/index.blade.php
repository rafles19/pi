@extends('backend.layout.template')
@push('css')
    <link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap5.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.5.0/css/responsive.bootstrap5.min.css">
@endpush

@section('content')
    {{-- Content --}}
    <main id="main" class="main">
        <div class="pagetitle">
            <h1>Invoice</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item">Components</li>
                    <li class="breadcrumb-item active">Invoice</li>
                </ol>
            </nav>
        </div>

        <div class="mt-3">
            <section class="section">
                <section class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="my-3">
                                    <div class="my-3">
                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif

                                        @if (session('success'))
                                            <div class="alert alert-success">
                                                {{ session('success') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                
                                {{-- <div class="float-end">
                                    <div class="">
                                        <div class="form-group">
                                            <a class="btn btn-success" href="{{ route('faq.create') }}">Create Faq</a>
                                        </div>    
                                    </div>
                                </div> --}}
                                
                                <h5 class="card-title-button">List Invoice</h5>
                                <table class="table table-striped wrap" style="width:100%" id="invoice-table">
                                    <thead class="background-color:blue">
                                        <tr>
                                            <th scope="col">Invoice Code</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Total</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </div><!-- End Page Title -->
    </main>
    @include('backend.invoice.deleteModal')
@endsection

@push('js')
    <script>
        function deleteInvoice(e) {
            let id = e.getAttribute('data-id');

            $('#deleteModal').modal('show');
            $('#confirmDeleteBtn').on('click', function() {

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'DELETE',
                    url: '/invoice/' + id,
                    dataType: 'json',
                    success: function(response) {
                        alert(response.message);
                        // Redirect or perform any other actions as needed
                        window.location.href = '/faq';
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(xhr.status + '\n' + xhr.responseText + '\n' + thrownError);
                    }
                });
                $('#deleteModal').modal('hide');
            });
        }
    </script>
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.5.0/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.5.0/js/responsive.bootstrap5.min.js"></script>

    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/vendor/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Template Main JS File -->
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script>
        $(document).ready(function() {
            // Inisialisasi DataTable
            var table = $('#invoice-table').DataTable({
                processing: true,
                serverSide: true,
                responsive:true,
                ajax: "{{ route('invoice.index') }}",
                columns: [
                    {
                        data: 'invoice_code',
                        name: 'invoice_code',
                        "width": "20%"
                    },
                    {
                        data: 'status_code',
                        name: 'status_code',
                        "width": "20%"
                    },
                    {
                        data: 'total',
                        name: 'total',
                        "width": "20%"
                    },
                    {
                        data: 'button',
                        name: 'button',
                        "width": "20%"
                    },
                ],
                order: [
                    [0, 'asc'], [1, 'asc']
                ],
            });

            $('#invoice-table tbody').on('click', 'tr', function() {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                } else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });
        });
    </script>
@endpush
