@extends('backend.layout.template')
@section('content')
    {{-- Content --}}
    <main id="main" class="main">
        <div class="pagetitle">
            <h1>Create About</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item">Components</li>
                    <li class="breadcrumb-item"><a href="{{ route('about.index') }}">About</a></li>
                    </li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>
            </nav>
        </div>

        <section class="section">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Please fill this form</h5>

                            {{-- form --}}
                            <form class="row g-3" action="{{ url('about') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="col-md-6">
                                    <label class="form-label">Section</label>
                                    <div class="col-sm-30">
                                        <input type="text" name="section" id="section"
                                            class="form-control @error('section') is-invalid @enderror"
                                            value="{{ old('section') }}">
                                        <div class="mt-3">
                                            @error('section')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="sequence" class="form-label">Sequence</label>
                                    <div class="col-sm-30">
                                        <input type="number" name="sequence" id="sequence"
                                            class="form-control @error('sequence') is-invalid @enderror"
                                            value="{{ old('sequence') }}">
                                        <div class="mt-3">
                                            @error('sequence')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label class="form-label">Title</label>
                                    <div class="col-sm-30">
                                        <input type="text" name="title" id="title"
                                            class="form-control @error('title') is-invalid @enderror"
                                            value="{{ old('title') }}">
                                        <div class="mt-3">
                                            @error('title')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label class="form-label">Link Url</label>
                                    <div class="col-sm-30">
                                        <input type="text" name="link_url" id="link_url"
                                            class="form-control @error('link_url') is-invalid @enderror"
                                            value="{{ old('link_url') }}">
                                        <div class="mt-3">
                                            @error('link_url')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="image" class="form-label">Image</label>
                                    <div class="col-sm-30">
                                        <input type="file" name="image" id="image" class="form-control">

                                        <div class="mt-3">
                                            @error('image')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label class="form-label">Button Link</label>
                                    <div class="col-sm-30">
                                        <input type="text" name="button_link" id="button_link"
                                            class="form-control @error('button_link') is-invalid @enderror"
                                            value="{{ old('button_link') }}">
                                        <div class="mt-3">
                                            @error('section')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <label class="form-label">Description</label>
                                    <!-- TinyMCE Editor -->
                                    <textarea id="description" name="description" class="tinymce-editor"></textarea><!-- End TinyMCE Editor -->
                                    <div class="mt-3">
                                        @error('description')
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <i class="bi bi-exclamation-octagon me-1"></i>
                                                {{ $message }}
                                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                    aria-label="Close"></button>
                                            </div>
                                        @enderror
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <label class="form-label">Status</label>
                                    <div class="col-sm-30">
                                        <select aria-label="Pilih Status" name="status" id="status"
                                            class="form-select">
                                            <option value="{{ old('status') }}" selected>Choose</option>
                                            <option value="1">Publish</option>
                                            <option value="0">Draft</option>
                                        </select>
                                        <div class="mt-3">
                                            @error('status')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="float-end"> 
                                        <div class="mt-3">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Create About</button>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@push('js')
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>

    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/tinymce/tinymce.min.js') }}"></script>

    <!-- Template Main JS File -->
    <script src="{{ asset('assets/js/main.js') }}"></script>
@endpush
