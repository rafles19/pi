@extends('backend.layout.template')
@section('content')
    {{-- Content --}}
    <main id="main" class="main">
        <div class="pagetitle">
            <h1>Create Article</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item">Components</li>
                    <li class="breadcrumb-item"><a href="{{ route('article.index') }}">Article</a></li>
                    </li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>
            </nav>
        </div>

        <section class="section">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Please fill this form</h5>

                            {{-- form --}}
                            <form class="row g-3" action="{{ url('article') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="col-md-6">
                                    <label for="title" class="form-label">Title</label>
                                    <div class="col-sm-30">
                                        <input type="text" name="title" id="title"
                                            class="form-control @error('title') is-invalid @enderror"
                                            value="{{ old('title') }}">
                                        <div class="mt-3">
                                            @error('title')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="slug_input" class="form-label">Slug</label>
                                    <div class="col-sm-12">
                                        <input class="form-control" type="text" name="slug" 
                                            id="slug" readonly>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="lead" class="form-label">Lead</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="lead" id="lead" class="form-control"
                                            value="{{ old('lead') }}">
                                        <div class="mt-3">
                                            @error('lead')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="category_id" class="form-label">Category</label>
                                    <div class="col-sm-12">
                                        <select name="category_id" id="category_id" class="form-control">
                                            <option value="{{ old('category_id') }}" hidden>Choose</option>
                                            @foreach ($categories as $item)
                                                <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                            @endforeach
                                        </select>
                                        <div class="mt-3">
                                            @error('category_id')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="keywords" class="form-label">Keywords</label>
                                    <div class="col-sm-12">
                                        <input type="text" id="keywords" name="keywords" class="form-control"
                                            value="{{ old('keywords') }}">
                                        <div class="mt-3">
                                            @error('keywords')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="image" class="form-label">Image</label>
                                    <div class="col-sm-12">
                                        <input type="file" name="image" id="image" class="form-control">
                                        <div class="mt-3">
                                            @error('image')
                                                <div class="alert alert-danger alert-dismissible fade show"
                                                    role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <!-- TinyMCE Editor -->
                                    <label for="description" class="form-label">Description</label>
                                    <textarea id="description" name="description" class="tinymce-editor"></textarea><!-- End TinyMCE Editor -->
                                    <div class="mt-3">
                                        @error('description')
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <i class="bi bi-exclamation-octagon me-1"></i>
                                                {{ $message }}
                                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                    aria-label="Close"></button>
                                            </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label class="form-label">Status</label>
                                    <div class="col-sm-12">
                                        <select aria-label="Pilih Status" name="status" id="status"
                                            class="form-select">
                                            <option value="{{ old('status') }}" selected>Choose</option>
                                            <option value="1">Publish</option>
                                            <option value="0">Draft</option>
                                        </select>
                                        <div class="mt-3">
                                            @error('status')
                                                <div class="alert alert-danger alert-dismissible fade show"
                                                    role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="published_date" class="form-label">Published Date</label>
                                    <input type="date" value="{{ old('published_date') }}" name="published_date"
                                        id="published_date" class="form-control">
                                    <div class="mt-3">
                                        @error('published_date')
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <i class="bi bi-exclamation-octagon me-1"></i>
                                                {{ $message }}
                                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                    aria-label="Close"></button>
                                            </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="float-end"> 
                                        <div class="mt-3">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Create Article</button>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@push('js')
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>

    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/simple-datatables/simple-datatables.js') }}"></script>
    <script src="{{ asset('assets/vendor/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.slugify.js') }}" type="text/javascript"></script>
    <!-- Template Main JS File -->
    <script src="{{ asset('assets/js/main.js') }}"></script>

    <script type="text/javascript" charset="utf-8">
        $().ready(function() {
            $('#slug').slugify('#title');
        });
    </script>
@endpush
