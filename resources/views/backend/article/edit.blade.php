@extends('backend.layout.template')
@section('content')
    <main id="main" class="main">
        <div class="pagetitle">
            <h1>Edit Article</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item">Components</li>
                    <li class="breadcrumb-item"><a href="{{ route('article.index') }}">Article</a></li>
                    </li>
                    <li class="breadcrumb-item active">update</li>
                </ol>
            </nav>
        </div>

        <section class="section">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Please review this form</h5>

                            {{-- form --}}
                            <form class="row g-3" action="{{ route('article.update', $article->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="col-md-6">
                                    <label for="title" class="form-label">Title</label>
                                    <div class="col-sm-30">
                                        <input type="text" name="title" id="title" class="form-control"
                                            value="{{ $article->title }}">
                                        <div class="mt-3">
                                            @if (session('success'))
                                                <div class="alert alert-success">
                                                    {{ session('success') }}
                                                </div>
                                            @endif

                                            @if (session('error'))
                                                <div class="alert alert-danger">
                                                    {{ session('error') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="slug_input" class="form-label">Slug</label>
                                    <div class="col-sm-30">
                                        <input class="form-control" type="text" name="slug" placeholder="{{ $article->slug }}"
                                            id="slug" readonly>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="category_id" class="form-label">Kategori</label>
                                    <div class="col-sm-30">
                                        <select aria-label="Pilih Category" name="category_id" id="category_id"
                                            class="form-select">
                                            <option value="">Choose</option>
                                            @foreach ($categories as $category)
                                                <option {{ $category->id == $article->category_id ? 'selected' : '' }}
                                                    value="{{ $category->id }}">{{ $category->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="lead" class="form-label">Lead</label>
                                    <div class="col-sm-30">
                                        <input type="text" name="lead" id="lead" class="form-control"
                                            value="{{ $article->lead }}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="keywords" class="form-label">Keywords</label>
                                    <div class="col-sm-12">
                                        <input type="text" id="keywords" name="keywords" class="form-control"
                                        value="{{ $article->keywords }}">
                                        <div class="mt-3">
                                            @error('keywords')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="image" class="form-label">Image</label>
                                    <div class="col-sm-12">
                                        <input type="file" name="image" id="image" class="form-control">
                                        <div class="mt-3">
                                            @error('image')
                                                <div class="alert alert-danger alert-dismissible fade show"
                                                    role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <label for="description" class="form-label">Description</label>
                                    <textarea name="description" id="description" cols="30" rows="10" class="tinymce-editor">{{ $article->description }}</textarea>
                                </div>

                                <div class="col-md-6">
                                    <label class="form-label" for="status">Status</label>
                                    <div class="col-sm-30">
                                        <select aria-label="Pilih Status" name="status" id="status"
                                            class="form-select">
                                            <option value="" hidden>Choose</option>
                                            <option value="1" {{ $article->status == 1 ? 'selected' : '' }}>Published
                                            </option>
                                            <option value="0" {{ $article->status == 0 ? 'selected' : '' }}>Draft
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label class="form-label" for="published_date">Published Date</label>
                                    <input value="{{ $article->published_date }}" type="date" name="published_date"
                                        id="published_date" class="form-control">
                                </div>

                                <div class="col-md-12">
                                    <div class="float-end"> 
                                        <div class="mt-3">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Update Article</button>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@push('js')
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>

    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/simple-datatables/simple-datatables.js') }}"></script>
    <script src="{{ asset('assets/vendor/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.slugify.js') }}" type="text/javascript"></script>
    <!-- Template Main JS File -->
    <script src="{{ asset('assets/js/main.js') }}"></script>

    <script>
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token=',
            clipboard_handleImages: false
        };

        document.addEventListener('DOMContentLoaded', function() {
            CKEDITOR.replace('myeditor', options);
        });
    </script>

    <script type="text/javascript" charset="utf-8">
        $().ready(function() {
            $('#slug').slugify('#title');
        });
    </script>
@endpush
