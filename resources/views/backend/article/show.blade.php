<!-- show.blade.php -->

@extends('backend.layout.template')

@section('title', 'Detail Artikel')
@section('content')
    {{-- Content --}}
    <main class="main" id="main"> <!-- Tambahkan class text-center di sini -->
        <div class="pagetitle">
            <h1>Article</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item">Components</li>
                    <li class="breadcrumb-item"><a href="{{ route('article.index') }}">Article</a></li>
                    <li class="breadcrumb-item active">Detail</li>
                </ol>
            </nav>
        </div>

        <div class="card mb-3 mx-auto">
            <div class="card-header">
                <div class="row mb-3">

                    <div class="col-12">
                        <time>
                            <span
                                class="text-13 text-md-16 text-lg-13 text-b text-grey card-date">{{ $article->published_date }}</span>
                        </time>


                        <div class="col-12">
                            <h1 class="text-26 text-md-28 text-lg-30 text-b mb-1 poppins-bold"><a href=""
                                    class="text-black">{{ $article->title }}</a></h1>
                        </div>

                        <div class="col-12">
                            @if ($article->image)
                                <img src="{{ asset('storage/' . $article->image) }}" class="credit"
                                    style="border-radius: 50%; width: 25px; height: 25px;">
                            @endif

                            <b class="editor">
                                <a href="" class="text-grey text-13">{{ $article->author }}</a>
                            </b>
                        </div>
                    </div>


                </div>
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-12 text-14 text-lg-16 text-black text-news-detail poppins-bold">
                            <p class="card-text">{!! $article->description !!}</p>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-12">
                            <div class="light-grey p-3">
                                <div class="row">
                                    <div class="col-12">
                                        <h5 class="text-18 text-b text-red mb-2 poppins-bold">Kanal</h5>
                                        <ul class="list-unstyled mb-0">
                                            <li class="px-1 float-left">
                                                <a href=""
                                                    class="text-13 text-b text-black poppins-semiBold">{{ $article->category->nama }}</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="{{ route('article.index') }}" class="btn btn-primary">Back to List</a>
                </div>
            </div>
    </main>
@endsection

@push('js')
    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/vendor/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Template Main JS File -->
    <script src="{{ asset('assets/js/main.js') }}"></script>
@endpush
