@extends('backend.layout.template')
@section('content')
    {{-- Content --}}
    <main id="main" class="main">
        <div class="pagenama">
            <h1>Create Category</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item">Components</li>
                    <li class="breadcrumb-item"><a href="{{ route('categories.index') }}">Category</a></li>
                    </li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>
            </nav>
        </div>

        <section class="section">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Please fill this form</h5>

                            {{-- form --}}
                            <form class="row g-3" action="{{ url('categories') }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="col-md-6">
                                    <label for="nama" class="col-form-label">Category Name</label>
                                    <div class="col-sm-30">
                                        <input type="text" name="nama" id="nama"
                                            class="form-control @error('nama') is-invalid @enderror"
                                            value="{{ old('nama') }}">
                                        <div class="mt-3">
                                            @error('nama')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="slug_input" class="col-sm-2 col-form-label">Slug</label>
                                    <div class="col-sm-30">
                                        <input class="form-control" type="text" name="slug" value=""
                                            id="slug" readonly>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-30">
                                        <select aria-label="Pilih Status" name="status" id="status" class="form-select">
                                            <option value="{{ old('status') }}" selected>Choose</option>
                                            <option value="1">Publish</option>
                                            <option value="0">Draft</option>
                                        </select>
                                        <div class="mt-3">
                                            @error('status')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="float-end"> 
                                        <div class="mt-3">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Create Category</button>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@push('js')
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>

    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.slugify.js') }}" type="text/javascript"></script>

    <!-- Template Main JS File -->
    <script src="{{ asset('assets/js/main.js') }}"></script>

    <script type="text/javascript" charset="utf-8">
        $().ready(function() {
            $('#slug').slugify('#nama');
        });
    </script>
@endpush
