@extends('backend.layout.template')
@section('content')
    <main id="main" class="main">
        <div class="pagenama">
            <h1>Edit Category</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item">Components</li>
                    <li class="breadcrumb-item"><a href="{{ route('categories.index') }}">categories</a></li>
                    </li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>
            </nav>
        </div>

        <section class="section">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Please fill this form</h5>

                            {{-- form --}}
                            <form class="row g-3" action="{{ route('categories.update', $category->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')

                                <div class="col-md-6">
                                    <label for="nama" class="form-label">Nama</label>
                                    <div class="col-sm-30">
                                        <input type="text" name="nama" id="nama" class="form-control"
                                            value="{{ $category->nama }}">
                                    </div>
                                    <div class="mt-3">
                                        @if (session('error'))
                                        <div class="alert alert-danger">
                                            {{ session('error') }}
                                        </div>
                                    @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="slug_input" class="form-label">Slug</label>
                                    <div class="col-sm-30">
                                        <input class="form-control" type="text" name="slug"
                                            placeholder="{{ $category->slug }}" id="slug" readonly>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label class="form-label" for="status">Status</label>
                                    <div class="col-sm-30">
                                        <select aria-label="Pilih Status" name="status" id="status"
                                            class="form-select">
                                            <option value="" hidden>Choose</option>
                                            <option value="1" {{ $category->status == 1 ? 'selected' : '' }}>
                                                Published</option>
                                            <option value="0" {{ $category->status == 0 ? 'selected' : '' }}>Draft
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="float-end"> 
                                        <div class="mt-3">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Update Category</button>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@push('js')
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>

    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.slugify.js') }}" type="text/javascript"></script>
    <!-- Template Main JS File -->
    <script src="{{ asset('assets/js/main.js') }}"></script>

    <script>
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token=',
            clipboard_handleImages: false
        };

        document.addEventListener('DOMContentLoaded', function() {
            CKEDITOR.replace('myeditor', options);
        });
    </script>

    <script type="text/javascript" charset="utf-8">
        $().ready(function() {
            $('#slug').slugify('#nama');
        });
    </script>
@endpush
