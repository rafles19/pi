@extends('layouts.app')

@section('content')
<h1>Detail Produk</h1>

<table class="table">
    <tbody>
        <tr>
            <th>Nama</th>
            <td>{{ $product->name }}</td>
        </tr>
        <tr>
            <th>Harga</th>
            <td>{{ $product->price }}</td>
        </tr>
        <tr>
            <th>Deskripsi</th>
            <td>{{ $product->description }}</td>
        </tr>
        <tr>
            <th>Gambar</th>
            <td>
                @foreach ($product->images as $image)
                <img src="{{ asset('storage/' . $image->url) }}" width="100">
                @endforeach
            </td>
        </tr>
    </tbody>
</table>

<a href="{{ route('products.index') }}">Kembali ke Daftar Produk</a>
@endsection
