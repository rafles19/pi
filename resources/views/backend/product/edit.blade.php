@extends('backend.layout.template')
@section('content')
    {{-- Content --}}
    <main id="main" class="main">
        <div class="pagetitle">
            <h1>Edit Product</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item">Components</li>
                    <li class="breadcrumb-item"><a href="{{ route('product') }}">Product</a></li>
                    </li>
                    <li class="breadcrumb-item active">Edit</li>
                </ol>
            </nav>
        </div>

        <section class="section">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Please fill this form</h5>

                            <!-- Bordered Tabs -->
                            <ul class="nav nav-tabs nav-tabs-bordered" id="borderedTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab"
                                        data-bs-target="#bordered-home" type="button" role="tab" aria-controls="home"
                                        aria-selected="true">Product Information</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="profile-tab" data-bs-toggle="tab"
                                        data-bs-target="#bordered-profile" type="button" role="tab"
                                        aria-controls="profile" aria-selected="false">Product Image</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="contact-tab" data-bs-toggle="tab"
                                        data-bs-target="#bordered-contact" type="button" role="tab"
                                        aria-controls="contact" aria-selected="false">Contact</button>
                                </li>
                            </ul>
                            <div class="tab-content pt-2" id="borderedTabContent">
                                <div class="tab-pane fade show active" id="bordered-home" role="tabpanel"
                                    aria-labelledby="home-tab">

                                    <div class="mt-3">
                                        {{-- form --}}
                                        <form class="row g-3" action="{{ route('update_product', $product->id) }}"
                                            method="POST" enctype="multipart/form-data">
                                            @csrf
                                            @method('PUT')
                                            <div class="col-md-6">
                                                <label class="form-label">Nama Product</label>
                                                <div class="col-sm-30">
                                                    <input type="text" name="name" id="name"
                                                        class="form-control @error('name') is-invalid @enderror"
                                                        value="{{ $product->name }}">
                                                    <div class="mt-3">
                                                        @error('name')
                                                            <div class="alert alert-danger alert-dismissible fade show"
                                                                role="alert">
                                                                <i class="bi bi-exclamation-octagon me-1"></i>
                                                                {{ $message }}
                                                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                                    aria-label="Close"></button>
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <label for="slug_input" class="form-label">Slug</label>
                                                <div class="col-sm-12">
                                                    <input class="form-control" type="text" name="slug" id="slug"
                                                        value="{{ $product->slug }}" readonly>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <label for="category_id" class="form-label">Kategori</label>
                                                <div class="col-sm-30">
                                                    <select aria-label="Pilih Category" name="category_id" id="category_id"
                                                        class="form-select">
                                                        <option value="">Choose</option>
                                                        @foreach ($cat_product as $category)
                                                            <option
                                                                {{ $category->id == $product->category_id ? 'selected' : '' }}
                                                                value="{{ $category->id }}">{{ $category->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <label class="form-label">Harga</label>
                                                <div class="col-sm-30">
                                                    <input type="number" name="price" id="price"
                                                        class="form-control @error('price') is-invalid @enderror"
                                                        value="{{ $product->price }}">
                                                    <div class="mt-3">
                                                        @error('price')
                                                            <div class="alert alert-danger alert-dismissible fade show"
                                                                role="alert">
                                                                <i class="bi bi-exclamation-octagon me-1"></i>
                                                                {{ $message }}
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="alert" aria-label="Close"></button>
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <label class="form-label">Stock</label>
                                                <div class="col-sm-30">
                                                    <input type="number" name="stock" id="stock"
                                                        class="form-control @error('stock') is-invalid @enderror"
                                                        value="{{ $product->stock }}">
                                                    <div class="mt-3">
                                                        @error('stock')
                                                            <div class="alert alert-danger alert-dismissible fade show"
                                                                role="alert">
                                                                <i class="bi bi-exclamation-octagon me-1"></i>
                                                                {{ $message }}
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="alert" aria-label="Close"></button>
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <label class="form-label">Berat (Gram)</label>
                                                <div class="col-sm-30">
                                                    <input type="number" name="weight" id="weight"
                                                        class="form-control @error('weight') is-invalid @enderror"
                                                        value="{{ $product->weight }}">
                                                    <div class="mt-3">
                                                        @error('weight')
                                                            <div class="alert alert-danger alert-dismissible fade show"
                                                                role="alert">
                                                                <i class="bi bi-exclamation-octagon me-1"></i>
                                                                {{ $message }}
                                                                <button type="button" class="btn-close"
                                                                    data-bs-dismiss="alert" aria-label="Close"></button>
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-4">
                                                <label for="image" class="form-label">Image 1</label>
                                                <div id="preview-images1"></div>
                                                <input type="file" name="images[]" id="image1"
                                                    class="form-control" style="width: 100%">
                                                <div class="mt-3">
                                                    @error('image')
                                                        <div class="alert alert-danger alert-dismissible fade show"
                                                            role="alert">
                                                            <i class="bi bi-exclamation-octagon me-1"></i>
                                                            {{ $message }}
                                                            <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                                aria-label="Close"></button>
                                                        </div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <label for="image" class="form-label">Image 2</label>
                                                <div id="preview-images2"></div>
                                                <input type="file" name="images[]" id="image2"
                                                    class="form-control">
                                                <div class="mt-3">
                                                    @error('image')
                                                        <div class="alert alert-danger alert-dismissible fade show"
                                                            role="alert">
                                                            <i class="bi bi-exclamation-octagon me-1"></i>
                                                            {{ $message }}
                                                            <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                                aria-label="Close"></button>
                                                        </div>
                                                    @enderror
                                                </div>
                                            </div>


                                            <div class="col-md-4">



                                                <label for="image" class="form-label">Image 3</label>

                                                <div class="col-sm-30">
                                                </div>
                                                <input type="file" name="images[]" id="image3"
                                                    class="form-control">
                                                <div class="mt-3">
                                                    @error('image')
                                                        <div class="alert alert-danger alert-dismissible fade show"
                                                            role="alert">
                                                            <i class="bi bi-exclamation-octagon me-1"></i>
                                                            {{ $message }}
                                                            <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                                aria-label="Close"></button>
                                                        </div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <!-- TinyMCE Editor -->
                                                <label for="description" class="form-label">Description</label>
                                                <textarea id="description" name="description" class="tinymce-editor">{{ $product->description }}</textarea><!-- End TinyMCE Editor -->
                                                <div class="mt-3">
                                                    @error('description')
                                                        <div class="alert alert-danger alert-dismissible fade show"
                                                            role="alert">
                                                            <i class="bi bi-exclamation-octagon me-1"></i>
                                                            {{ $message }}
                                                            <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                                aria-label="Close"></button>
                                                        </div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <label class="form-label" for="status">Status</label>
                                                <div class="col-sm-30">
                                                    <select aria-label="Pilih Status" name="status" id="status"
                                                        class="form-select">
                                                        <option value="" hidden>Choose</option>
                                                        <option value="1"
                                                            {{ $product->status == 1 ? 'selected' : '' }}>Published
                                                        </option>
                                                        <option value="0"
                                                            {{ $product->status == 0 ? 'selected' : '' }}>Draft
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>



                                            <div class="col-md-12">
                                                <div class="float-end">
                                                    <div class="mt-3">
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-primary">Create
                                                                Link</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="bordered-profile" role="tabpanel"
                                    aria-labelledby="profile-tab">
                                    @if ($product->images->count() > 0)
                                        <h4>Gambar Produk</h4>
                                        <div class="row">
                                            @foreach ($product->images as $image)
                                                <div class="col-md-3">
                                                    <img src="{{ Storage::url($image->image) }}" class="img-thumbnail"
                                                        alt="{{ $product->name }}">
                                                    <form action="{{ route('destroy', $image->id) }}" method="post">
                                                        @csrf
                                                        @method('delete')
                                                        <button type="submit"
                                                            class="btn btn-danger btn-sm">Hapus</button>
                                                    </form>
                                                </div>
                                            @endforeach
                                        </div>
                                    @else
                                        <p>Produk tidak memiliki gambar.</p>
                                    @endif
                                </div>
                                <div class="tab-pane fade" id="bordered-contact" role="tabpanel"
                                    aria-labelledby="contact-tab">
                                    Saepe animi et soluta ad odit soluta sunt. Nihil quos omnis animi debitis cumque.
                                    Accusantium quibusdam perspiciatis qui qui omnis magnam. Officiis accusamus impedit
                                    molestias nostrum veniam. Qui amet ipsum iure. Dignissimos fuga tempore dolor.
                                </div>
                            </div><!-- End Bordered Tabs -->

                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@push('js')
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>

    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/simple-datatables/simple-datatables.js') }}"></script>
    <script src="{{ asset('assets/vendor/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.slugify.js') }}" type="text/javascript"></script>
    <!-- Template Main JS File -->
    <script src="{{ asset('assets/js/main.js') }}"></script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('#name').on('input', function() {
                var slug = slugify($(this).val());
                $('#slug').val(slug);
            });
        });

        function slugify(text) {
            return text.toString().toLowerCase()
                .replace(/\s+/g, '-')           
                .replace(/[^\w\-]+/g, '')       
                .replace(/\-\-+/g, '-')         
                .replace(/^-+/, '')             
                .replace(/-+$/, '');            
        }
    </script>
    <script>
        $(document).ready(function() {
            // Menambahkan event listener pada setiap input file
            $("input[name='images[]']").change(function() {
                // Mendapatkan file yang dipilih
                var files = $(this)[0].files;

                // Mendapatkan id dari input file yang sedang digunakan
                var inputId = $(this).attr('id');

                // Mendapatkan id untuk pratinjau gambar sesuai dengan input file yang sedang digunakan
                var previewId = '#preview-images' + inputId.slice(-
                1); // Mengambil digit terakhir dari id input sebagai bagian dari id pratinjau

                // Mengosongkan pratinjau gambar sebelum menambahkan yang baru
                $(previewId).empty();

                // Looping untuk setiap file yang dipilih
                for (var i = 0; i < files.length; i++) {
                    // Membaca file sebagai data URL
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        // Menampilkan preview gambar
                        var img = '<img src="' + e.target.result + '" width="100">';
                        $(previewId).append(img);
                    };
                    reader.readAsDataURL(files[i]);
                }
            });
        });
    </script>
    
@endpush
