@extends('backend.layout.template')
@section('content')
    @push('css')
        <link rel="stylesheet" href="{{ asset('assets/extra/image-galery.css') }}">
    @endpush
    {{-- Content --}}
    <main id="main" class="main">
        <div class="pagetitle">
            <h1>Add Product</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item">Components</li>
                    <li class="breadcrumb-item"><a href="{{ url('/product') }}">Product</a></li>
                    </li>
                    <li class="breadcrumb-item active">Create</li>
                </ol>
            </nav>
        </div>

        <section class="section">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Please fill this form</h5>

                            {{-- form --}}
                            <form class="row g-3" action="{{ url('product/add') }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="col-md-6">
                                    <label class="form-label">Nama Product</label>
                                    <div class="col-sm-30">
                                        <input type="text" name="name" id="name"
                                            class="form-control @error('name') is-invalid @enderror"
                                            value="{{ old('name') }}">
                                        <div class="mt-3">
                                            @error('name')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="slug_input" class="form-label">Slug</label>
                                    <div class="col-sm-12">
                                        <input class="form-control" type="text" name="slug" id="slug" readonly>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="category_id" class="form-label">Category</label>
                                    <div class="col-sm-12">
                                        <select name="category_id" id="category_id" class="form-control">
                                            <option value="{{ old('category_id') }}" hidden>Choose</option>
                                            @foreach ($cat_product as $item)
                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="mt-3">
                                            @error('category_id')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class="form-label">Harga</label>
                                    <div class="col-sm-30">
                                        <input type="number" name="price" id="price"
                                            class="form-control @error('price') is-invalid @enderror"
                                            value="{{ old('price') }}">
                                        <div class="mt-3">
                                            @error('price')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class="form-label">Stock</label>
                                    <div class="col-sm-30">
                                        <input type="number" name="stock" id="stock"
                                            class="form-control @error('stock') is-invalid @enderror"
                                            value="{{ old('stock') }}">
                                        <div class="mt-3">
                                            @error('stock')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class="form-label">Berat (Gram)</label>
                                    <div class="col-sm-30">
                                        <input type="number" name="weight" id="weight"
                                            class="form-control @error('weight') is-invalid @enderror"
                                            value="{{ old('weight') }}">
                                        <div class="mt-3">
                                            @error('weight')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>


                                <div class="col-4">
                                    <label for="image" class="form-label">Image 1</label>
                                    <div id="preview-images1"></div>
                                    <input type="file" name="images[]" id="image1" class="form-control"
                                        style="width: 100%">
                                    <div class="mt-3">
                                        @error('image')
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <i class="bi bi-exclamation-octagon me-1"></i>
                                                {{ $message }}
                                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                    aria-label="Close"></button>
                                            </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label for="image" class="form-label">Image 2</label>
                                    <div id="preview-images2"></div>
                                    <input type="file" name="images[]" id="image2" class="form-control">
                                    <div class="mt-3">
                                        @error('image')
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <i class="bi bi-exclamation-octagon me-1"></i>
                                                {{ $message }}
                                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                    aria-label="Close"></button>
                                            </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label for="image" class="form-label">Image 3</label>
                                    <div id="preview-images3"></div>
                                    <div class="col-sm-30">
                                    </div>
                                    <input type="file" name="images[]" id="image3" class="form-control">
                                    <div class="mt-3">
                                        @error('image')
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <i class="bi bi-exclamation-octagon me-1"></i>
                                                {{ $message }}
                                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                    aria-label="Close"></button>
                                            </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <!-- TinyMCE Editor -->
                                    <label for="description" class="form-label">Description</label>
                                    <textarea id="description" name="description" class="tinymce-editor"></textarea><!-- End TinyMCE Editor -->
                                    <div class="mt-3">
                                        @error('description')
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <i class="bi bi-exclamation-octagon me-1"></i>
                                                {{ $message }}
                                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                    aria-label="Close"></button>
                                            </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label class="form-label">Status</label>
                                    <div class="col-sm-12">
                                        <select aria-label="Pilih Status" name="status" id="status"
                                            class="form-select">
                                            <option value="{{ old('status') }}" selected>Choose</option>
                                            <option value="1">Publish</option>
                                            <option value="0">Draft</option>
                                        </select>
                                        <div class="mt-3">
                                            @error('status')
                                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <i class="bi bi-exclamation-octagon me-1"></i>
                                                    {{ $message }}
                                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                        aria-label="Close"></button>
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="float-end">
                                        <div class="mt-3">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Create Product</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@push('js')
    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.slugify.js') }}" type="text/javascript"></script>


    <!-- Template Main JS File -->
    <script src="{{ asset('assets/js/main.js') }}"></script>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#name').on('input', function() {
                var slug = slugify($(this).val());
                $('#slug').val(slug);
            });
        });

        function slugify(text) {
            return text.toString().toLowerCase()
                .replace(/\s+/g, '-')           
                .replace(/[^\w\-]+/g, '')       
                .replace(/\-\-+/g, '-')         
                .replace(/^-+/, '')             
                .replace(/-+$/, '');            
        }
    </script>
    <script>
        $(document).ready(function() {
            // Menambahkan event listener pada setiap input file
            $("input[name='images[]']").change(function() {
                // Mendapatkan file yang dipilih
                var files = $(this)[0].files;

                // Mendapatkan id dari input file yang sedang digunakan
                var inputId = $(this).attr('id');

                // Mendapatkan id untuk pratinjau gambar sesuai dengan input file yang sedang digunakan
                var previewId = '#preview-images' + inputId.slice(-
                    1); // Mengambil digit terakhir dari id input sebagai bagian dari id pratinjau

                // Mengosongkan pratinjau gambar sebelum menambahkan yang baru
                $(previewId).empty();

                // Looping untuk setiap file yang dipilih
                for (var i = 0; i < files.length; i++) {
                    // Membaca file sebagai data URL
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        // Menampilkan preview gambar
                        var img = '<img src="' + e.target.result + '" width="100">';
                        $(previewId).append(img);
                    };
                    reader.readAsDataURL(files[i]);
                }
            });
        });
    </script>

    
@endpush
