@foreach ($cat_products as $item)
    <!-- Modal -->
    <div class="modal fade" id="modalUpdate{{ $item->id }}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-success text-white">
                    <h1 class="modal-title fs-5" id="staticBackdropLabel">Update Category</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('category-product/'.$item->id) }}" method="post">
                        @method('PUT')
                        @csrf
                        <div class="mb-3">
                            <label for="name">name</label>
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ $item->name }}">
                            @error('name', $item->name)
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="mt-3">
                            <label class="form-label">Status</label>
                            <select aria-label="Pilih Status" name="status" id="status"
                                                class="form-select">
                                                <option value="" hidden>Choose</option>
                                                <option value="1" {{ $item->status == 1 ? 'selected' : '' }}>
                                                    Published</option>
                                                <option value="0" {{ $item->status == 0 ? 'selected' : '' }}>Draft
                                                </option>
                                            </select>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endforeach
