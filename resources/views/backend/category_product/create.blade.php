<!-- Button trigger modal -->

  
  <!-- Modal -->
  <div class="modal fade" id="modalCreate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Category</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form action="{{ url('category-product') }}" method="POST">
            @csrf
            <div>
                <label for="name" class="form-label fw-bold">Nama Category</label>
                <input class="form-control" type="text" name="name" id="name">
            </div>
            
            <div class="mt-3">
                <label class="form-label">Status</label>
                <select aria-label="Pilih Status" name="status" id="status"
                                            class="form-select">
                                            <option value="{{ old('status') }}" selected>Choose</option>
                                            <option value="1">Publish</option>
                                            <option value="0">Draft</option>
                                        </select>
            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>

          </form>
        </div>
        
      </div>
    </div>
  </div>