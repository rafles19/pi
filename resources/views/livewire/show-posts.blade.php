<div class="offset-3 col-6">
    <h1 class="h5">test</h1>
    <div class="mb-3">
        <select wire:model.live="selectedsection" class="form-select" name="" id="">
            <option value="">Pilih</option>
            @foreach ($section as $sections)
                <option value="{{ $sections }}">{{ $sections }}</option>
            @endforeach
        </select>
    </div>
    @if (!is_null($selectedsection))
        <div class="mb-3">
            <select wire:model.live="selectedsequence" class="form-select" name="" id="">
                <option value="">Pilih</option>
                @foreach ($sequence as $seq)
                    <option value="{{ $seq }}">{{ $seq }}</option> 
                @endforeach
            </select>
        </div>
    @endif
</div>
