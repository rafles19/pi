@extends('role.layout')

@section('content')
    <style>
        #myform {
            text-align: center;
            padding: 5px;
            border: 1px dotted #ccc;
            margin: 2%;
        }

        .qty {
            width: 40px;
            height: 25px;
            text-align: center;
        }

        input.qtyplus {
            width: 25px;
            height: 25px;
        }

        input.qtyminus {
            width: 25px;
            height: 25px;
        }
    </style>

    <h1>Detail Produk</h1>

    <ul>
        @foreach ($product->images as $image)
            <div class="col-md-3">
                <img src="{{ Storage::url($image->image) }}" class="img-thumbnail" alt="{{ $product->name }}">
            </div>
        @endforeach
        <li>Nama Produk: {{ $product->name }}</li>
        <li>Harga: {{ $product->price }}</li>
        <li>Deskripsi: {!! $product->description !!}</li>
    </ul>

    <form id='myform' class='quantity' action="{{ route('cart.store') }}" method="post">
        @csrf
        <input type='button' value='-' class='qtyminus minus' field='quantity' />
        <input type='text' name='quantity' value='1' class='qty' />
        <input type='button' value='+' class='qtyplus plus' field='quantity' />
        <input type="hidden" name="slug" value="{{ $product->slug }}">
        <button type="submit">Tambahkan ke Keranjang</button>
    </form>

    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script>
        jQuery(document).ready(($) => {
            $('.quantity').on('click', '.plus', function(e) {
                let $input = $(this).prev('input.qty');
                let val = parseInt($input.val());
                $input.val(val + 1).change();
            });

            $('.quantity').on('click', '.minus', function(e) {
                let $input = $(this).next('input.qty');
                var val = parseInt($input.val());
                if (val > 1) {
                    $input.val(val - 1).change();
                }
            });
        });
    </script>
@endsection
