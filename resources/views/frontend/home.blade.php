@extends('role.layout')

@section('content')
<div class="card">
    <div class="card-body">
        <!-- Slides with captions -->
        <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="assets/img/slides-1.jpg" class="d-block mx-auto" style="width: 2560px; height: 1440px;" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>First slide label</h5>
                        <p>Some representative placeholder content for the first slide.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="assets/img/slides-2.jpg" class="d-block mx-auto" style="width: 2560px; height: 1440px;" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Second slide label</h5>
                        <p>Some representative placeholder content for the second slide.</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="assets/img/slides-3.jpg" class="d-block mx-auto" style="width: 2560px; height: 1440px;" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Third slide label</h5>
                        <p>Some representative placeholder content for the third slide.</p>
                    </div>
                </div>
            </div>

            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>

        </div><!-- End Slides with captions -->
    </div>
</div>



    <main id="main" class="main">
        <section class="section">
            <div class="row">
                <div class="col-lg-12">
                    <!-- kategori -->
                    <div class="card mx-auto" style="padding: 20px; background-color: #ADC178; border:none;">
                        <div class="bg-transparent">
                            <h2 class="text-center" style="font-weight:bold; margin-bottom: 20px;">Product Category</h2>
                            <div class="row justify-content-center">
                                @foreach ($itemCategories as $kategori)
                                <div class="card m-2" style="width: 18rem;">
                                    <img class="card-img-top" src="..." alt="Card image cap">
                                    <div class="card-body">
                                        <h1>{{ $kategori->name }}</h1>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    
                    <!-- end kategori -->

                    <div class="card">
                        <div class="card-body">
                            <h2>Featured Products</h2>

                            

                            <div class="row">
                                @foreach ($itemProducts as $product)
                                    <div class="col-md-4 mb-4">
                                        <div class="card" style="width: 100%;">
                                            <img class="card-img-top" src="..." alt="Product Image">
                                            <div class="card-body">
                                                <h5 class="card-title">{{ $product->name }}</h5>
                                                <h5 class="card-title">Rp {{ $product->price }}</h5>
                                                <form id='myform' class='quantity' action="{{ route('cart.store') }}"
                                                    method="post">
                                                    @csrf
                                                    <div class="input-group">
                                                        <button type='button' class='btn btn-outline-secondary minus'
                                                            field='quantity'>-</button>
                                                        <input type='text' name='quantity' value='1'
                                                            class='form-control qty' />
                                                        <button type='button' class='btn btn-outline-secondary plus'
                                                            field='quantity'>+</button>
                                                    </div>
                                                    <input type="hidden" name="slug" value="{{ $product->slug }}">
                                                    <button type="submit" class="btn btn-primary mt-2">Add to Cart</button>
                                                </form>

                                                <a href="{{ route('show', $product->slug) }}" class="btn btn-info">View
                                                    Details</a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            <div class="row">
                                @foreach ($itemCategoriesHome as $cathome)
                                    <h2>Categories {{ $cathome->category->name }}</h2>
                                    <div class="col-md-4 mb-4">
                                        <div class="card" style="width: 100%;">
                                            <img class="card-img-top" src="..." alt="Product Image">
                                            <div class="card-body">
                                                <h5 class="card-title">{{ $cathome->name }}</h5>
                                                <h5 class="card-title">Rp {{ $cathome->price }}</h5>
                                                <form id='myform' class='quantity' action="{{ route('cart.store') }}"
                                                    method="post">
                                                    @csrf
                                                    <div class="input-group">
                                                        <button type='button' class='btn btn-outline-secondary minus'
                                                            field='quantity'>-</button>
                                                        <input type='text' name='quantity' value='1'
                                                            class='form-control qty' />
                                                        <button type='button' class='btn btn-outline-secondary plus'
                                                            field='quantity'>+</button>
                                                    </div>
                                                    <input type="hidden" name="slug" value="{{ $cathome->slug }}">
                                                    <button type="submit" class="btn btn-primary mt-2">Add to Cart</button>
                                                </form>

                                                <a href="{{ route('show', $cathome->slug) }}" class="btn btn-info">View
                                                    Details</a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <h2>All Products</h2>
    <ul>
        <!-- List all products here if needed -->
    </ul>

    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script>
        jQuery(document).ready(($) => {
            $('.quantity').on('click', '.plus', function(e) {
                let $input = $(this).prev('input.qty');
                let val = parseInt($input.val());
                $input.val(val + 1).change();
            });

            $('.quantity').on('click', '.minus', function(e) {
                let $input = $(this).next('input.qty');
                var val = parseInt($input.val());
                if (val > 1) {
                    $input.val(val - 1).change();
                }
            });
        });
    </script>
@endsection
