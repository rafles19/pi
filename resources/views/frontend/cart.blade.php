@extends('role.layout')

@section('content')
    <h1>Your Cart</h1>

    <div class="container">
        @if (count($cartItems) > 0)
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Product</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Price</th>
                        <th scope="col">Total</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($cartItems as $item)
                        <tr>
                            <td><a href="{{ route('show', $item->product->slug) }}">{{ $item->product->name }}</a></td>
                            <td>{{ $item->quantity }}</td>
                            <td>{{ $item->price }}</td>
                            <td>{{ $item->subtotal }}</td>
                            <td>
                                <form id='myform' class='quantity' action="{{ route('up.cart', $item->product->slug) }}"
                                    method="post">
                                    @csrf
                                    <div class="input-group">
                                        <button type='button' class='btn btn-outline-secondary minus'
                                            field='quantity'>-</button>
                                        <input type='text' name='quantity' value='1' class='form-control qty' />
                                        <button type='button' class='btn btn-outline-secondary plus'
                                            field='quantity'>+</button>
                                    </div>
                                    <input type="hidden" name="slug" value="{{ $item->product->slug }}">
                                    <button type="submit" class="btn btn-primary mt-2">Update Quantity</button>

                                </form>
                            </td>
                            <td>
                                <form action="{{ route('cart.delete', $item->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Yes, delete</button>
                                    <a href="{{ url()->previous() }}" class="btn btn-default">Cancel</a>
                                </form>

                            </td>
                            <td>
                                <form action="{{ route('cart.delete.all') }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Yes, delete all</button>
                                    <a href="{{ url()->previous() }}" class="btn btn-default">Cancel</a>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div>

            </div>
            <div class="card">
                <div class="card-body">
                    <h2>Cart Summary</h2>
                    <p>Total Items: {{ $totalItems }}</p>
                    <p>Subtotal: {{ $subtotal }}</p>
                    <p>Tax (10): {{ $tax }}</p>
                    <p>Total: {{ $total_payment }}</p>

                    <form action="{{ route('checkout') }}" method="POST">
                        @csrf
                        <button type="submit">Buat Pesanan</button>
                    </form>




                    {{-- <form action="{{ route('checkout', $cartItems->product->slug) }}" method="post">
                        @csrf

                        

                        <button type="submit" class="btn btn-primary">Proceed to Checkout</button>
                    </form> --}}


                </div>
            </div>
        @else
            <p>Your cart is empty.</p>
        @endif
    </div>

    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script>
        jQuery(document).ready(($) => {
            $('.quantity').on('click', '.plus', function(e) {
                let $input = $(this).prev('input.qty');
                let val = parseInt($input.val());
                $input.val(val + 1).change();
            });

            $('.quantity').on('click', '.minus', function(e) {
                let $input = $(this).next('input.qty');
                var val = parseInt($input.val());
                if (val > 1) {
                    $input.val(val - 1).change();
                }
            });
        });
    </script>

@endsection

@push('js')
    <script>
        function removeItemFromCart(rowId) {
            $('#rowId_D').val(rowId);
            $('#deleteFromCart').submit();
        }
    </script>
@endpush
