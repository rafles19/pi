// public/js/article.js atau resources/assets/js/article.js

function deleteArticle(e) {
    let id = e.getAttribute('data-id');

    if (confirm('Apakah Anda yakin ingin menghapus artikel ini?')) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'DELETE',
            url: '/article/' + id,
            dataType: 'json',
            success: function(response) {
                alert(response.message);
                window.location.href = '/article';
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.status + '\n' + xhr.responseText + '\n' + thrownError);
            }
        });
    }
}

$(document).ready(function() {
    $('#dataTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('article.index') }}',
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' },
            { data: 'title', name: 'title' },
            { data: 'category_id', name: 'category_id' },
            { data: 'views', name: 'views' },
            { data: 'status', name: 'status' },
            { data: 'published_date', name: 'published_date' },
            { data: 'button', name: 'button' },
        ]
    });
});
