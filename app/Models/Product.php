<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';

    protected $fillable = ['category_id', 'name', 'price', 'weight', 'stock', 'description', 'status', 'slug'];

    public function Category(): BelongsTo
    {
        return $this->belongsTo(CatProduct::class);
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    

    public function getRouteKeyName()
    {
        return 'slug';
    } 
}
