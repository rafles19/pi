<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use HasFactory;

    protected $fillable = ['section', 'title', 'sequence', 'image_desktop', 'image_mobile', 'link_url', 'status'];
}
