<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    use HasFactory;
    protected $fillable = ['sequence','section', 'title', 'description', 'image_url', 'button_link', 'link_url', 'status'];    
}
