<?php

namespace App\Livewire;

use App\Models\About;
use App\Models\County;
use App\Models\Subcounty;
use Livewire\Component;

class ShowPosts extends Component
{
    public $section;
    public $selectedsection = null;
    public $sequence;

    public function mount()
    {
        $this->section = About::pluck('section')->unique();
    }

    public function updatedSelectedsection()
    {
        $this->sequence = About::where('section', $this->selectedsection)->orderBy('sequence')->distinct()->pluck('sequence');
    }
    
    public function render()
    {
        return view('livewire.show-posts')->with('sequences', $this->sequence);
    }
}
