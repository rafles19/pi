<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Invoice;
use App\Models\Product;

use Illuminate\Http\Request;


class CartController extends Controller
{

    public function index()
    {
        $user = auth()->user();
        $cartItems = Cart::where('user_id', $user->id)->get();
        $totalItems = $cartItems->unique('product_id')->count();
        $subtotal = Cart::sum('subtotal');
        $tax = $subtotal * 0.1;
        $total_payment = $subtotal + $tax;

        return view('frontend.cart', compact('cartItems', 'totalItems', 'subtotal', 'tax', 'total_payment'));
    }




    public function updateQty(Request $request, string $slug)
    {
        $product = Product::where('slug', $slug)->first();

        // Check if the product exists and handle if not found
        if (!$product) {
            return redirect()->back()->withErrors(['product' => 'Product not found']);
        }

        // Validate user login
        if (!auth()->user()) {
            return redirect()->route('login');
        }

        $user = auth()->user();
        $newQuantity = $request->quantity;
        $subtotal = $product->price * $newQuantity;

        $cart = Cart::where('user_id', $user->id)
            ->where('product_id', $product->id)
            ->first();

        // Update quantity and subtotal of existing cart item
        if ($cart) {
            try {
                $cart->quantity = $newQuantity;
                $cart->subtotal = $subtotal;
                $cart->save();
                return back()->with('success', 'Cart updated successfully');
            } catch (\Exception $e) {
                return back()->withErrors(['error' => $e->getMessage()]);
            }
        }

        return redirect()->back()->withErrors(['cart' => 'Cart item not found']);
    }



    public function show(String $id)
    {
        $cart = Cart::findOrFail($id);
        // dd($product);

        // Periksa jika produk ditemukan
        if (!$cart) {
            return abort(404, 'Product not found');
        }

        // Tampilkan view dengan data produk
        return view('product.cartDetails', compact('cart'));
    }



    public function store(Request $request)
    {
        // Ambil slug dari permintaan dan cari produk
        $slug = $request->slug;
        $product = Product::where('slug', $slug)->first();


        if (!$product) {
            return redirect()->back()->withErrors(['product' => 'Produk tidak ditemukan']);
        }

        // Validasi 
        if (!auth()->user()) {
            return redirect()->route('login');
        }

        $user = auth()->user();
        $cart = Cart::where('user_id', $user->id)
            ->where('product_id', $product->id)
            ->first();

        $subtotal = $product->price * $request->quantity;

        if (!$cart) {
            try {
                Cart::create([
                    'user_id' => $user->id,
                    'product_id' => $product->id,
                    'quantity' => $request->quantity,
                    'price' => $product->price,
                    'subtotal' => $subtotal,
                ]);
                return back()->with('success', 'Item keranjang berhasil ditambahkan');
            } catch (\Exception $e) {
                return back()->withErrors(['error' => $e->getMessage()]);
            }
        } else {
            try {
                $cart->quantity += $request->quantity;
                $cart->subtotal += $subtotal;
                $cart->save();
                return back()->with('success', 'Update keranjang berhasil');
            } catch (\Exception $e) {
                return back()->withErrors(['error' => $e->getMessage()]);
            }
        }
    }

    public function deleteCartItem(Request $request, $cartId)
    {
        $user = auth()->user();
        $cart = Cart::where('user_id', $user->id)
            ->where('id', $cartId)
            ->first();

        if (!$cart) {
            return response()->json([
                'status' => 'error',
                'message' => 'Cart item not found'
            ]);
        }

        $cart->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Cart item deleted successfully'
        ]);
    }


    public function deleteAllUserCartItems(Request $request)
    {
        $user = auth()->user();

        // Delete all cart items belonging to the user
        Cart::where('user_id', $user->id)->delete();

        // Return a JSON response indicating success
        return response()->json([
            'status' => 'success',
            'message' => 'All cart items belonging to the user deleted successfully.',
        ]);
    }


    public function checkOut(Request $request)
    {
        // Autentikasi pengguna untuk memastikan akses yang sah
        $user = auth()->user();
        if (!$user) {
            return response()->json(['error' => 'Akses tidak sah'], 401);
        }

        // Ambil semua item keranjang pengguna
        $cartItems = Cart::where('user_id', $user->id)->get();

        // Validasi stok produk
        foreach ($cartItems as $cartItem) {
            $product = $cartItem->product;
            if ($product->stock < $cartItem->quantity) {
                return response()->json(['error' => 'Stok produk ' . $product->name . ' tidak mencukupi'], 400);
            }
        }

        // Kurangi stok produk
        foreach ($cartItems as $cartItem) {
            $product = $cartItem->product;
            $product->stock -= $cartItem->quantity;
            $product->save();
        }

        // Simpan data invoice ke database
        $invoices = [];
        foreach ($cartItems as $cartItem) {
            $product = $cartItem->product;
            $invoice = Invoice::create([
                'user_id' => $user->id,
                'product_id' => $product->id,
                'invoice_code' => uniqid(), // Contoh, gunakan kode unik untuk invoice
                'status_code' => 'PENDING', // Status pembayaran, bisa disesuaikan
                'status_pengiriman' => 'PENDING', // Status pengiriman, bisa disesuaikan
                'no_resi' => null, // Nomor resi akan diisi setelah pengiriman
                'kurir' => null, // Nama kurir akan diisi setelah pengiriman
                'quantity' => $cartItem->quantity,
                'total' => $product->price * $cartItem->quantity,
            ]);
            $invoices[] = $invoice;
        }

        // Hapus item keranjang setelah checkout
        Cart::where('user_id', $user->id)->delete();

        // Kembalikan respons sukses
        return response()->json([
            'message' => 'Checkout berhasil',
            'invoices' => $invoices,
        ]);
    }
}
