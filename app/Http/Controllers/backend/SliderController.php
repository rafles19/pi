<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\SliderRequest;
use App\Http\Requests\UpdateSliderRequest;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) { 
            $sliders = Slider::all();
            return DataTables::of($sliders)
                ->addColumn('status', function ($article) {
                    if ($article->status == 0) {
                        return ' <span class="badge bg-info text-dark"><i class="ri-draft-line me-1"></i> Draft</span>';
                    } else {
                        return '<span class="badge bg-success"><i class="ri-bookmark-3-fill me-1"></i> Published</span>';
                    }
                })
                ->addColumn('button', function ($sliders) {
                    return '
                    <a class="ri-edit-box-line" href="slider/' . $sliders->id . '/edit"></a>
                    <a class="delete ri-delete-bin-5-line" href="#" onclick="deleteSlider(this)" data-id="' . $sliders->id . ' "></a>
                    ';
                })
                ->rawColumns(['button', 'status'])
                ->make(true);
        }
        return view('backend.slider.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SliderRequest $request)
    {

        $data = $request->validated();

        if ($request->hasFile('image')) {
            $filename = time() . '_' . $request->image->getClientOriginalName();
            $filePath = $request->image->storeAs('slider', $filename);
            $data['image_desktop'] = $filePath;
            $data['image_mobile'] = $filePath;
        }
        
        Slider::create($data);
        return redirect(route('slider.index'))->with('success', 'Slider Telah Dibuat');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $slider = Slider::findOrFail($id);
       ;
        return view('backend.slider.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateSliderRequest $request, string $id)
    {

        $slider = Slider::findOrFail($id);
       
        $data = $request->validated();

        if ($request->hasFile('image')) {
            $request->validate([
                'image' => ['required', 'max:2028', 'image'],
            ]);

            // Hapus gambar lama jika ada
            if ($slider->image_desktop && $slider->image_mobile) {
                Storage::delete($slider->image_desktop);
            }

            // Simpan gambar baru
            $filename = time() . '_' . $request->image->getClientOriginalName();
            $filePath = $request->image->storeAs('slider', $filename);
            $data['image_desktop'] = $filePath;
            $data['image_mobile'] = $filePath;
        }

        

        $slider->update($data);
        return redirect(route('slider.index'))->with('success', 'Data saved');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $slider = Slider::findOrFail($id);
            if (!empty($slider->image_desktop) && Storage::exists($slider->image_desktop)) {
                Storage::delete($slider->image_desktop);
            }
            $slider->delete();
            return response()->json(['success' => true, 'message' => 'Artikel berhasil dihapus.']);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'Artikel tidak ditemukan.'], 404);
        }
    }
}
