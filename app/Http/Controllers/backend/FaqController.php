<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\FaqRequest;
use App\Http\Requests\UpdateFaqRequest;
use App\Models\Faq;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $faqs = Faq::all();

            return DataTables::of($faqs)
                ->addColumn('status', function ($faq) {
                    if ($faq->status == 0) {
                        return ' <span class="badge bg-info text-dark"><i class="ri-draft-line me-1"></i> Draft</span>';
                    } else {
                        return '<span class="badge bg-success"><i class="ri-bookmark-3-fill me-1"></i> Published</span>';
                    }
                })
                ->addColumn('button', function ($faq) {
                    return '
                        <a class="ri-edit-box-line" href="' . route('faq.edit', $faq->id) . '"></a>
                        <a class="delete ri-delete-bin-5-line" href="#" onclick="deleteFaq(this)" data-id="' . $faq->id . '"></a>
                    ';
                })
                ->rawColumns(['button', 'status'])
                ->make(true);
        }

        return view('backend.faq.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(FaqRequest $request)
    {
        $data = $request->validated();

        if ($request->hasFile('image')) {
            $filename = time() . '_' . $request->image->getClientOriginalName();
            $filePath = $request->image->storeAs('faq', $filename);
            $data['image'] = $filePath;
        }

        Faq::create($data);

        return redirect(route('faq.index'))->with('success', 'Faq Telah Dibuat');
    }

    /**
     * Show the form for editing the specified resource.
     */

    public function edit(string $id)
    {

        $faq = Faq::findOrFail($id);
        // $section = $faq->section;
        // $faqs = Faq::where('section', $section)->get();
        // $allSequences = Faq::where('section', $section)->pluck('sequence');

        return view('backend.faq.edit', compact('faq'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateFaqRequest $request, string $id)
{
    $faq = Faq::findOrFail($id);
    // $section = $request->input('section');
    $data = $request->validated();

    if ($request->hasFile('image')) {
        $request->validate([
            'image' => ['required', 'max:2028', 'image'],
        ]);

        // Hapus gambar lama jika ada
        if ($faq->image) {
            Storage::delete($faq->image);
        }

        // Simpan gambar baru
        $filename = time() . '_' . $request->image->getClientOriginalName();
        $filePath = $request->image->storeAs('faq', $filename);
        $data['image'] = $filePath;
    }

    // Update FAQ setelah pertukaran (atau tanpa pertukaran)
    $faq->update($data);

    return redirect(route('faq.index'))->with('success', 'Update tersimpan');
}




    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $faq = Faq::findOrFail($id);
            if (!empty($faq->image) && Storage::exists($faq->image)) {
                Storage::delete($faq->image);
            }
            $faq->delete();
            return response()->json(['success' => true, 'message' => 'Artikel berhasil dihapus.']);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'Artikel tidak ditemukan.'], 404);
        }
    }
}
