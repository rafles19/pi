<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\LinkRequest;
use App\Http\Requests\UpdateLinkRequest;
use App\Models\Faq;
use App\Models\Link;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
use Yajra\DataTables\Facades\DataTables;

class LinkController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $links = Link::all();
            $title = 'Delete Data!';
            $text = "Are you sure you want to delete?";
            confirmDelete($title, $text);

            return DataTables::of($links)
                ->addColumn('status', function ($link) {
                    if ($link->status == 0) {
                        return ' <span class="badge bg-info text-dark"><i class="ri-draft-line me-1"></i> Draft</span>';
                    } else {
                        return '<span class="badge bg-success"><i class="ri-bookmark-3-fill me-1"></i> Published</span>';
                    }
                })
                ->addColumn('button', function ($link) {
                    return '
                    <a class="ri-edit-box-line" href="link/' . $link->id . '/edit"></a>
                    <a data-toggle="modal" data-target="#deleteModal" class="delete ri-delete-bin-5-line" href="#" onclick="deleteLink(this)" data-id="' . $link->id . ' "></a>
                    ';
                })
                ->rawColumns(['button', 'status'])
                ->make(true);
        }


        return view('backend.link.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.link.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(LinkRequest $request)
    {

        $data = $request->validated();

        if ($request->hasFile('image')) {
            $filename = time() . '_' . $request->image->getClientOriginalName();
            $filePath = $request->image->storeAs('link', $filename);
            $data['icon_url'] = $filePath;
        }
        Link::create($data);
        Alert::success('Hore!', 'Link Created Successfully');
        return redirect(route('link.index'))->with('success', 'Link Telah Dibuat');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $link = Link::findOrFail($id);

        return view('backend.link.edit', compact('link'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateLinkRequest $request, string $id)
    {
        $link = Link::findOrFail($id);


        $data = $request->validated();

        if ($request->hasFile('image')) {
            $request->validate([
                'image' => ['required', 'max:2028', 'image'],
            ]);

            // Hapus gambar lama jika ada
            if ($link->image) {
                Storage::delete($link->image);
            }

            // Simpan gambar baru
            $filename = time() . '_' . $request->image->getClientOriginalName();
            $filePath = $request->image->storeAs('link', $filename);
            $data['icon_url'] = $filePath;
        }

        $link->update($data);
        Alert::success('Hore!', 'Link Updated Successfully');
        return redirect(route('link.index'))->with('success', 'Update tersimpan');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $link = Link::find($id);

        if (!$link) {
            return response()->json(['error' => 'Artikel tidak ditemukan.'], 404);
        }

        if (!empty($link->icon_url) && Storage::exists($link->icon_url)) {
            Storage::delete($link->icon_url);
        }

        $link->delete();

        alert()->success('Hore!', 'Post Deleted Successfully');

        return response()->json(['success' => true, 'message' => 'Artikel berhasil dihapus.']);
    }
}
