<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class InvoiceController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $invoices = Invoice::all();

            return DataTables::of($invoices)
                ->addColumn('status_code', function ($invoice) {
                    if ($invoice->status_code == 0) {
                        return ' <span class="badge bg-info text-dark"><i class="ri-draft-line me-1"></i> Draft</span>';
                    } else {
                        return '<span class="badge bg-success"><i class="ri-bookmark-3-fill me-1"></i> Published</span>';
                    }
                })
                ->addColumn('button', function ($invoice) {
                    return '
                    <a class="ri-edit-box-line" href="invoice/' . $invoice->id . '/edit"></a>
                    <a data-toggle="modal" data-target="#deleteModal" class="delete ri-delete-bin-5-line"  href="#" onclick="deleteInvoice(this)" data-id="' . $invoice->id . ' "></a>
                    ';
                })
                ->rawColumns(['button', 'status_code'])
                ->make(true);
        }
        return view('backend.invoice.index');
    }

    public function edit(Invoice $invoice)
    {
        $users = User::all();
        $products = Product::all();

        return view('backend.invoice.edit', compact('invoice', 'users', 'products'));
    }

    public function update(Request $request, Invoice $invoice)
    {
        $validator = Validator::make($request->all(), [
            'status_code' => 'required|string',
            'status_pengiriman' => 'required|string|in:PENDING,SHIPPED,DELIVERED',
            'no_resi' => 'nullable|string',
            'kurir' => 'nullable|string',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $invoice->update($request->all());

        return redirect()->route('invoice.index')->with('success', 'Invoice berhasil diubah.');
    }

    public function destroy(string $id)
    {
        $invoice = Invoice::find($id);

        if (!$invoice) {
            return response()->json(['error' => 'Invoice tidak ditemukan.'], 404);
        }

        // if (!empty($invoice->icon_url) && Storage::exists($link->icon_url)) {
        //     Storage::delete($link->icon_url);
        // }

        $invoice->delete();

        alert()->success('Hore!', 'Post Deleted Successfully');

        return response()->json(['success' => true, 'message' => 'Artikel berhasil dihapus.']);
    }
}
