<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Models\Category;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
    /**
     * Menampilkan daftar data kategori.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $categories = Category::all();

            return DataTables::of($categories)
                ->addColumn('status', function ($category) {
                    if ($category->status == 0) {
                        return ' <span class="badge bg-info text-dark"><i class="ri-draft-line me-1"></i> Draft</span>';
                    } else {
                        return '<span class="badge bg-success"><i class="ri-bookmark-3-fill me-1"></i> Published</span>';
                    }
                })
                ->addColumn('button', function ($category) {
                    return '
                    <a class="ri-edit-box-line" href="categories/' . $category->id . '/edit"></a>
                    <a data-toggle="modal" data-target="#deleteModal" class="delete ri-delete-bin-5-line" href="#" onclick="deleteCategories(this)" data-id="' . $category->id . ' "></a>
                    ';
                })
                ->rawColumns(['button', 'status'])
                ->make(true);
        }

        return view('backend.category.index');
    }

    public function create()
    {
        return view('backend.category.create', [
            'categories' => Category::get()
        ]);
    }

    /**
     * Menyimpan data kategori baru.
     */
    public function store(CategoryRequest $request)
    {
        $data = $request->validated();
        $slug = Str::slug($data['nama']);

        // Valoidasi ketika slugnya ternyata wajib unique/tidak boleh sama
        // disini yang dilakukan bukan menambahkan bilangan uniq dibelakang slugnya, tetapi memang title tersebut tidak dapat diambil
        if (Category::where('slug', $slug)->exists()) {
            return redirect(route('categories.create'))->with('error', 'Category name already exists. Please change the category name.');
        }

        $data['slug'] = $slug;

        Category::create($data);
        return redirect(route('categories.index'))->with('success', 'Kategori Telah Dibuat');
    }

    /**
     * Menampilkan formulir untuk mengedit data kategori.
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('backend.category.edit', compact('category'));
    }

    /**
     * Mengupdate data kategori yang sudah ada.
     */
    public function update(UpdateCategoryRequest $request, $id)
    {
        $category = Category::findOrFail($id);

        $data = $request->validated();
        $slug = Str::slug($data['nama']);

        // Pengecekan/validasi halaman edit
        if (Category::where('slug', $slug)->where('id', '!=', $id)->exists()) {
            return redirect(route('categories.edit', ['category' => $id]))->with('error', 'Category name sudah ada. Silakan ubah Category.');
        } elseif ($category->slug == $slug) {
            return redirect(route('categories.index'))->with('success', 'Data saved');
        }

        $data['slug'] = $slug;

        $category->update($data);
        return redirect(route('categories.index'))->with('success', 'Update tersimpan');
    }

    /**
     * Menghapus data kategori.
     */
    public function destroy($id)
    {
        try {
            $category = Category::findOrFail($id);
            $category->delete();
            return response()->json(['success' => true, 'message' => 'Kategori berhasil dihapus.']);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'Kategori tidak ditemukan.'], 404);
        }
    }
}
