<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\SettingRequest;
use App\Http\Requests\UpdateSettingRequest;
use Illuminate\Support\Str;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) { 
            $settings = Setting::all();
            return DataTables::of($settings)
                ->addIndexColumn()
                ->addColumn('status', function ($article) {
                    if ($article->status == 0) {
                        return ' <span class="badge bg-info text-dark"><i class="ri-draft-line me-1"></i> Draft</span>';
                    } else {
                        return '<span class="badge bg-success"><i class="ri-bookmark-3-fill me-1"></i> Published</span>';
                    }
                })
                ->addColumn('button', function ($settings) {
                    return '
                    <a class="ri-edit-box-line" href="setting/' . $settings->id . '/edit"></a>
                    <a data-toggle="modal" data-target="#deleteModal" class="delete ri-delete-bin-5-line" href="#" onclick="deleteSetting(this)" data-id="' . $settings->id . ' "></a>
                    ';
                })
                ->rawColumns(['button', 'status'])
                ->make(true);
        }
        return view('backend.setting.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.setting.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SettingRequest $request)
    {
        $data = $request->validated();

        if ($request->hasFile('image')) {
            $filename = time() . '_' . $request->image->getClientOriginalName();
            $filePath = $request->image->storeAs('setting', $filename);
            $data['param'] = $filePath;
        }

        $slug = Str::slug($data['name']);
        // Validasi ketika slugnya ternyata wajib unique/tidak boleh sama
        // disini yang dilakukan bukan menambahkan bilangan uniq dibelakang slugnya, tetapi memang title tersebut tidak dapat diambil
        if (Setting::where('slug', $slug)->exists()) {
            return redirect(route('setting.create'))->with('error', 'Title already exists. Please change the title.');
        }
    
        $data['slug'] = $slug;
        Setting::create($data);
        return redirect(route('setting.index'))->with('success', 'Setting Telah Dibuat');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $setting = Setting::findOrFail($id);
        return view('backend.setting.edit', compact('setting'));

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateSettingRequest $request, string $id)
    {
        $setting = Setting::findOrFail($id);

        $data = $request->validated();

        if ($request->hasFile('image')) {
            $request->validate([
                'image' => ['required', 'max:2028', 'image'],
            ]);

            // Hapus gambar lama jika ada
            if ($setting->param) {
                Storage::delete($setting->param);
            }

            // Simpan gambar baru
            $filename = time() . '_' . $request->image->getClientOriginalName();
            $filePath = $request->image->storeAs('setting', $filename);
            $data['param'] = $filePath;
        }
        
        $slug = Str::slug($data['name']);

        // Pengecekan/validasi halaman edit
        if (Setting::where('slug', $slug)->where('id', '!=', $id)->exists()) {
            return redirect(route('setting.edit', ['setting' => $id]))->with('error', 'Name sudah ada. Silakan ubah Name.');
        } elseif ($setting->slug == $slug) {
            return redirect(route('setting.index'))->with('success', 'Data saved');
        }
    
        // Update the slug in the data array
        $data['slug'] = $slug;
    

        $setting->update($data);
        return redirect(route('setting.index'))->with('success', 'Update tersimpan');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $setting = Setting::findOrFail($id);
            if (!empty($setting->param) && Storage::exists($setting->param)) {
                Storage::delete($setting->param);
            }
            $setting->delete();
            return response()->json(['success' => true, 'message' => 'Artikel berhasil dihapus.']);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'Artikel tidak ditemukan.'], 404);
        }
    }
}
