<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use Illuminate\Support\Str;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Cart;
use App\Models\Category;
use App\Models\CatProduct;
use Illuminate\Support\Facades\Log;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Http\Client\Request as ClientRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (request()->ajax()) {
            $products = Product::with('category')->get();

            return DataTables::of($products)
                ->addIndexColumn()
                ->addColumn('category_id', function ($product) {
                    return $product->category ? $product->category->name : '-';
                })
                ->addColumn('status', function ($product) {
                    if ($product->status == 0) {
                        return ' <span class="badge bg-info text-dark"><i class="ri-draft-line me-1"></i> Draf</span>';
                    } else {
                        return '<span class="badge bg-success"><i class="ri-bookmark-3-fill me-1"></i> Dipublikasikan</span>';
                    }
                })
                ->addColumn('button', function ($product) {
                    return '
                    // <a href="article/' . $product->id . '" class="view ri-eye-line"></a>
                    <a class="ri-edit-box-line" href="product/' . $product->id . '/edit"></a>
                    <a data-toggle="modal" data-target="#deleteModal" class="delete ri-delete-bin-5-line" href="#" onclick="deleteProduct(this)" data-id="' . $product->id . ' "></a>
                ';
                })
                ->rawColumns(['category_id', 'status', 'button'])
                ->make();
        }
        return view('backend.product.index');
    }



    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.product.create', [
            'cat_product' => CatProduct::get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */



    // Helper function to ensure unique slug
    // private function isTitleUnique($title)
    // {
    //     return !Product::where('title', $title)->exists();
    // }


    public function store(ProductRequest $request)
{
    $data = $request->validated();
    $slug = Str::slug($data['name']);

    if (Product::where('slug', $slug)->exists()) {
        return redirect(route('product.create'))->with('error', 'Name already exists. Please change the title.');
    }

    $data['slug'] = $slug;
    
    $product = Product::create($data); // Use $data directly instead of $data->all()

    if ($request->hasFile('images')) {
        foreach ($request->file('images') as $image) {
            $productImage = new ProductImage();
            $productImage->product_id = $product->id;
            $productImage->image = $image->store('products');
            $productImage->save();
        }
    }

    return redirect()->route('product')->with('success', 'Product Created Successfully');
}


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        return view('backend.product.edit', [
            'product' => Product::findOrFail($id),
            'cat_product' => CatProduct::get(),
            'img_product' => ProductImage::get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProductRequest $request, string $id)
    {
        $product = Product::findOrFail($id);
        $data = $request->validated();

        


        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $image) {
                $productImage = new ProductImage();
                $productImage->product_id = $product->id;
                $productImage->image = $image->store('products');
                $productImage->save();
            }
        }

        $slug = Str::slug($data['name']);
        // Pengecekan/validasi halaman edit
        if (Product::where('slug', $slug)->where('id', '!=', $id)->exists()) {
            return redirect(route('product.edit', ['product' => $id]))->with('error', 'Title sudah ada. Silakan ubah judul.');
        } elseif ($product->slug == $slug) {
            return redirect(route('product'))->with('success', 'Data saved');
        }

        $data['slug'] = $slug;

        $product->update($data);


        return redirect(url('product'))->with('success', 'Artikel berhasil diupdate.');
    }



    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {

        $product = Product::find($id);
        if (!$product) {
            return redirect()->route('product')->with('error', 'Produk tidak ditemukan');
        }

        $productImages = ProductImage::where('product_id', $id)->get();
        foreach ($productImages as $productImage) {
            Storage::delete($productImage->image);
            $productImage->delete();
        }
        $product->delete();

        return redirect()->route('product')->with('success', 'Produk Berhasil Dihapus');
    }

    public function productHome()
    {
        
        $itemProducts = Product::orderBy('created_at', 'desc')->take(3)->get(); 
        $itemCategories = CatProduct::orderBy('created_at', 'desc')->take(3)->get();
        $itemCategoriesHome = Product::with('category')->orderBy('created_at', 'desc')->take(3)->get();
        
        return view('frontend.home', compact('itemProducts', 'itemCategories', 'itemCategoriesHome'));
    }

    public function productView($slug)
    {
        // Cari produk berdasarkan slug
        $product = Product::where('slug', $slug)->first();
        // dd($product);

        // Periksa jika produk ditemukan
        if (!$product) {
            // Handle product not found (redirect, error message, etc.)
            return abort(404, 'Product not found');
        }

        // Tampilkan view dengan data produk
        return view('frontend.productDetail', compact('product'));
    }
}
