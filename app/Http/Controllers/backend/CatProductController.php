<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CatProductRequests;
use App\Http\Requests\UpdateCatProductRequests;
use App\Models\CatProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\Contracts\DataTable;
use Yajra\DataTables\Facades\DataTables;

class CatProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $cat_products = CatProduct::all();

            return DataTables::of($cat_products)
                ->addColumn('status', function ($cat_product) {
                    if ($cat_product->status == 0) {
                        return '<span class="badge bg-info text-dark"><i class="ri-draft-line me-1"></i> Draft</span>';
                    } else {
                        return '<span class="badge bg-success"><i class="ri-bookmark-3-fill me-1"></i> Published</span>';
                    }
                })
                ->addColumn('button', function ($cat_product) {
                    return '<div class="text-center">
                    <a href="#" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#modalEdit" data-id="' . $cat_product->id . '" onclick="editCatproduct(this)">Edit</a>
                    <a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal" onclick="deleteCatproduct(this)" data-id="' . $cat_product->id . '">Hapus</a>
                </div>';
                })
                ->rawColumns(['button', 'status'])
                ->make(true);
        }

        $cat_products = CatProduct::latest()->get();
        return view('backend.category_product.index', compact('cat_products'));
    }



    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CatProductRequests $request)
    {
        $data = $request->validated();
        $slug = Str::slug($data['name']);

        if (CatProduct::where('slug', $slug)->exists()) {
            return redirect(route('categories.create'))->with('error', 'Category name already exists. Please change the category name.');
        }
        $data['slug'] = $slug;
        CatProduct::create($data);
        return redirect(route('category-product.index'))->with('success', 'Kategori Telah Dibuat');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCatProductRequests $request, string $id)
    {
        $cat_products =  CatProduct::findOrFail($id);
        $data = $request->validated();
        $slug = Str::slug($data['name']);

        // Pengecekan/validasi halaman edit
        if (CatProduct::where('slug', $slug)->where('id', '!=', $id)->exists()) {
            return redirect(route('category-product.edit', ['cat_products' => $id]))->with('error', 'Category name sudah ada. Silakan ubah Category.');
        } elseif ($cat_products->slug == $slug) {
            return redirect(route('category-product.index'))->with('success', 'Data saved');
        }

        $data['slug'] = $slug;

        $cat_products->update($data);
        return redirect(route('category-product.index'))->with('success', 'Update tersimpan');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        try {
            $cat_products = CatProduct::findOrFail($id);
            $cat_products->delete();
            return response()->json(['success' => true, 'message' => 'Kategori berhasil dihapus.']);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'Kategori tidak ditemukan.'], 404);
        }
    }
}
