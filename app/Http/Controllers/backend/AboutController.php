<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AboutRequest;
use App\Http\Requests\UpdateAboutRequest;
use App\Models\About;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $abouts = About::all();

            return DataTables::of($abouts)
                ->addColumn('status', function ($about) {
                    if ($about->status == 0) {
                        return ' <span class="badge bg-info text-dark"><i class="ri-draft-line me-1"></i> Draft</span>';
                    } else {
                        return '<span class="badge bg-success"><i class="ri-bookmark-3-fill me-1"></i> Published</span>';
                    }
                })
                ->addColumn('button', function ($about) {
                    return '
                    <a class="ri-edit-box-line" href="about/' . $about->id . '/edit"></a>
                    <a data-toggle="modal" data-target="#deleteModal" class="delete ri-delete-bin-5-line"  href="#" onclick="deleteAbout(this)" data-id="' . $about->id . ' "></a>
                    ';
                })
                ->rawColumns(['button', 'status'])
                ->make(true);
        }
        return view('backend.about.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.about.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(AboutRequest $request)
    {
        $data = $request->validated();

        if ($request->hasFile('image')) {
            $filename = time() . '_' . $request->image->getClientOriginalName();
            $filePath = $request->image->storeAs('about', $filename);
            $data['image_url'] = $filePath;
        }

        About::create($data);

        return redirect(route('about.index'))->with('success', 'About Telah Dibuat');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $about = About::findOrFail($id);

        return view('backend.about.edit', compact('about'));
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateAboutRequest $request, string $id)
    {
        $about = About::findOrFail($id);

        $data = $request->validated();
        if ($request->hasFile('image')) {
            $request->validate([
                'image' => ['required', 'max:2028', 'image'],
            ]);

            // Hapus gambar lama jika ada
            if ($about->image_url) {
                Storage::delete($about->image_url);
            }

            // Simpan gambar baru
            $filename = time() . '_' . $request->image->getClientOriginalName();
            $filePath = $request->image->storeAs('about', $filename);
            $data['image_url'] = $filePath;
        }

        $about->update($data);

        return redirect(route('about.index'))->with('success', 'Update tersimpan');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $about = About::findOrFail($id);

            if (!empty($about->image_url) && Storage::exists($about->image_url)) {
                Storage::delete($about->image_url);
            }

            $about->delete();
            return response()->json(['success' => true, 'message' => 'Artikel berhasil dihapus.']);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'Artikel tidak ditemukan.'], 404);
        }
    }
}
