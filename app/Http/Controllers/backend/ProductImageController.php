<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductImageController extends Controller
{
    public function destroy($id)
    {
        $productImage = ProductImage::findOrFail($id);
        Storage::delete($productImage->image);
        $productImage->delete();
        return redirect()->back()->with('success', 'Gambar produk berhasil dihapus');
    }
}
