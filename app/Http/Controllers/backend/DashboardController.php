<?php

namespace App\Http\Controllers\backend;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;
use Spatie\FlareClient\View;

class DashboardController extends Controller
{
    public function index()
    {
        return view('backend.dashboard.index', [
            'total_articles' => Article::count(),
            'total_categories' => Category::count(),
            'total_testimonials' => Article::where('category_id', 2)->count(),
            'latest' => Article::with('Category')->whereStatus('1')->latest()->get(),
            'latest_article' => Article::with('Category')->where('category_id', '!=', 2)->latest()->get(),
            
        ]);
    }
}
