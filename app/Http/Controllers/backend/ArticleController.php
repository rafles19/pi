<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Http\Requests\UpdateArticleRequest;
use App\Models\Article;
use App\Models\Category;
use Intervention\Image\Facades\Image;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

use function PHPUnit\Framework\returnSelf;

class ArticleController extends Controller
{

    public function index()
    {
        if (request()->ajax()) {
            $articles = Article::with('category')->where('category_id', '!=', 2)->latest()->get();


            return DataTables::of($articles)
                ->addIndexColumn()
                ->addColumn('category_id', function ($article) {
                    return $article->category->nama;
                })
                ->addColumn('status', function ($article) {
                    if ($article->status == 0) {
                        return ' <span class="badge bg-info text-dark"><i class="ri-draft-line me-1"></i> Draft</span>';
                    } else {
                        return '<span class="badge bg-success"><i class="ri-bookmark-3-fill me-1"></i> Published</span>';
                    }
                })
                ->addColumn('button', function ($article) {
                    return '
                            <a href="article/' . $article->id . '" class="view ri-eye-line"></a>
                            <a class="ri-edit-box-line" href="article/' . $article->id . '/edit"></a>
                            <a data-toggle="modal" data-target="#deleteModal" class="delete ri-delete-bin-5-line" href="#" onclick="deleteArticle(this)" data-id="' . $article->id . ' "></a>
                        ';
                })
                // yang akan ditampilkan
                ->rawColumns(['category_id', 'status', 'button'])
                ->make();
        }

        return view('backend.article.index');
    }



    // membuat article
    public function create()
    {
        return view('backend.article.create', [
            'categories' => Category::get()
        ]);
    }

    public function store(ArticleRequest $request)
    {
        $data = $request->validated();

        $filename = time() . '_' . $request->image->getClientOriginalName();
        $filePath = $request->image->storeAs('uploads', $filename);

        // $img = Image::make('uploads' . $filePath);
        // $img->resize(100, 100);
        // $img->save('uploads/thumbnails', $filename);

        $data['image'] = $filePath;
        $slug = Str::slug($data['title']);
        // $data['thumbnail_image'] = $thumbnailPath;
        $user = auth()->user();
        $data['author'] = $user->name;

        // Valoidasi ketika slugnya ternyata wajib unique/tidak boleh sama
        // disini yang dilakukan bukan menambahkan bilangan uniq dibelakang slugnya, tetapi memang title tersebut tidak dapat diambil
        if (Article::where('slug', $slug)->exists()) {
            return redirect(route('article.create'))->with('error', 'Title already exists. Please change the title.');
        }

        $data['slug'] = $slug;

        Article::create($data);

        return redirect(url('article'))->with('success', 'Article Has Been Created');
    }


    //lihat article
    public function show(string $id)
    {
        return view('backend.article.show', [
            'article' => Article::findOrFail($id)
        ]);

        // $article = Article::findOrFail($id);
    }

    // edit article
    public function edit(string $id)
    {
        return view('backend.article.edit', [
            'article' => Article::findOrFail($id),
            'categories' => Category::get()
        ]);
    }

    // simpan hasil edit ke database
    public function update(UpdateArticleRequest $request, string $id)
    {
        $article = Article::findOrFail($id);

        $data = $request->validated();

        // cek gambar
        if ($request->hasFile('image')) {
            $request->validate([
                'image' => ['required', 'max:2028', 'image'],
            ]);

            // Hapus gambar lama jika ada
            if ($article->image) {
                Storage::delete($article->image);
            }

            // Simpan gambar baru
            $filename = time() . '_' . $request->image->getClientOriginalName();
            $filePath = $request->image->storeAs('uploads', $filename);
            $data['image'] = $filePath;
        }

        $slug = Str::slug($data['title']);

        // Pengecekan/validasi halaman edit
        if (Article::where('slug', $slug)->where('id', '!=', $id)->exists()) {
            return redirect(route('article.edit', ['article' => $id]))->with('error', 'Title sudah ada. Silakan ubah judul.');
        } elseif ($article->slug == $slug) {
            return redirect(route('article.index'))->with('success', 'Data saved');
        }

        // Update the slug in the data array
        $data['slug'] = $slug;

        $data['author'] = auth()->user()->name;

        $article->update($data);

        return redirect(url('article'))->with('success', 'Artikel berhasil diupdate.');
    }



    //hapus
    public function destroy(string $id)
    {
        try {
            $article = Article::findOrFail($id);
            Storage::delete($article->image); // Hapus file gambar jika ada
            $article->delete();
            return response()->json(['success' => true, 'message' => 'Artikel berhasil dihapus.']);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'Artikel tidak ditemukan.'], 404);
        }
    }
}
