<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\TestimonialRequest;
use App\Http\Requests\UpdateTestimonialRequest;
use App\Models\Article;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (request()->ajax()) {
            $testimonials = Article::with('category')->where('category_id', 2)->latest()->get();

            return DataTables::of($testimonials)
                ->addIndexColumn()
                ->addColumn('button', function ($testimonial) {
                    return '
                        <a href="testimonial/' . $testimonial->id . '" class="btn btn-sm btn-info">Detail</a>
                        <a class="ri-edit-box-line" href="testimonial/' . $testimonial->id . '/edit">Edit</a>
                        <a class="delete ri-delete-bin-5-line" href="#" onclick="deleteTestimonial(this)" data-id="' . $testimonial->id . '">Delete</a>
                    ';
                })
                ->rawColumns(['button'])
                ->make();
        }

        return view('backend.testimonial.index');
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.testimonial.create', [
            'categories' => Category::get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TestimonialRequest $request)
    {
        $data = $request->validated();

        $data['slug'] = Str::slug('testimoni '. $data['impression']);
        $data['title'] = 'testimoni';
        $data['category_id'] = 2;

        $user = auth()->user();
        $data['author'] = $user->name;

        $data['stay_for'] = 'Stayed for ' . $data['jumlah_malam'] . 'night in ' . $data['bulan'];
        unset($data['jumlah_malam']);
        unset($data['bulan']);

        // dd($data);
        Article::create($data);

        return redirect(url('testimonial'))->with('success', 'Testimonial Has Been Created');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        return view('backend.testimonial.show', [
            'testimonial' => Article::findOrFail($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        return view('backend.testimonial.edit', [
            'article' => Article::findOrFail($id),
            'categories' => Category::get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTestimonialRequest $request, string $id)
    {
        $testimonial = Article::findOrFail($id);

        $data = $request->validated();

        $data['slug'] = Str::slug($data['impression']);
        $data['title'] = 'testimoni';
        $data['category_id'] = 2;

        $user = auth()->user();
        $data['author'] = $user->name;
        $data['stay_for'] = 'Stayed for ' . $data['jumlah_malam'] . 'night in ' . $data['bulan'];
        
        
        unset($data['jumlah_malam']);
        unset($data['bulan']);

        $testimonial->update($data);
        return redirect(url('testimonial'))->with('success', 'Testimonial berhasil diupdate.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $testimonial= Article::findOrFail($id);
            $testimonial->delete();
            return response()->json(['success' => true, 'message' => 'Artikel berhasil dihapus.']);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'Artikel tidak ditemukan.'], 404);
        }
    }
}
