<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\StaticPageRequest;
use App\Http\Requests\UpdateStaticPageRequest;
use App\Models\Static_Page;
use App\Models\StaticPage;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class StaticPageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $statics = StaticPage::all();
            return DataTables::of($statics)
                ->addIndexColumn()
                ->addColumn('status', function ($statics) {
                    if ($statics->status == 0) {
                        return '<span class="badge bg-info text-dark"><i class="ri-draft-line me-1"></i> Draft</span>';
                    } else {
                        return '<span class="badge bg-success"><i class="ri-bookmark-3-fill me-1"></i> Published</span>';
                    }
                })
                ->addColumn('button', function ($statics) {
                    return '
                                <a class="ri-edit-box-line" href="' . route('static-page.edit', $statics->id) . '"></a>
                                <a class="delete ri-delete-bin-5-line" href="#" onclick="deleteStatic(this)" data-id="' . $statics->id . '"></a>
                            ';
                })
                ->rawColumns(['button', 'status'])
                ->toJson(); // Menggunakan toJson() alih-alih make(true)
        }
        return view('backend.static_page.index');
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.static_page.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StaticPageRequest $request)
    {
        $data = $request->validated();

        if ($request->hasFile('image')) {
            $filename = time() . '_' . $request->image->getClientOriginalName();
            $filePath = $request->image->storeAs('static', $filename);
            $data['image'] = $filePath;
        }

        $slug = Str::slug($data['title']);

        // Valoidasi ketika slugnya ternyata wajib unique/tidak boleh sama
        // disini yang dilakukan bukan menambahkan bilangan uniq dibelakang slugnya, tetapi memang title tersebut tidak dapat diambil
        if (StaticPage::where('slug', $slug)->exists()) {
            return redirect(route('static-page.create'))->with('error', 'Title already exists. Please change the title.');
        }

        $data['slug'] = $slug;
        
        StaticPage::create($data);
        return redirect(route('static-page.index'))->with('success', 'Static Page Telah Dibuat');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $static = StaticPage::findOrFail($id);
        return view('backend.static_page.edit', compact('static'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateStaticPageRequest $request, string $id)
    {

        $static = StaticPage::findOrFail($id);
        $data = $request->validated();

        if ($request->hasFile('image')) {
            $request->validate([
                'image' => ['required', 'max:2028', 'image'],
            ]);

            // Hapus gambar lama jika ada
            if ($static->image) {
                Storage::delete($static->image);
            }

            // Simpan gambar baru
            $filename = time() . '_' . $request->image->getClientOriginalName();
            $filePath = $request->image->storeAs('static', $filename);
            $data['image'] = $filePath;
        }

        $slug = Str::slug($data['title']);

        // Pengecekan/validasi halaman edit
        if (StaticPage::where('slug', $slug)->where('id', '!=', $id)->exists()) {
            return redirect(route('static-page.edit', ['static_page' => $id]))->with('error', 'Judul sudah ada. Silakan ubah judul.');
        } elseif ($static->slug == $slug) {
            return redirect(route('static-page.index'))->with('success', 'Data saved');
        }
    
        // Update the slug in the data array
        $data['slug'] = $slug;

        $static->update($data);
        return redirect(route('static-page.index'))->with('success', 'Data saved');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $static = StaticPage::findOrFail($id);
            if (!empty($static->image) && Storage::exists($static->image)) {
                Storage::delete($static->image);
            }
            $static->delete();
            return response()->json(['success' => true, 'message' => 'Artikel berhasil dihapus.']);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['error' => 'Artikel tidak ditemukan.'], 404);
        }
    }
}
