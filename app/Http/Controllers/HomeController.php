<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartRequest;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Membuat instance controller baru.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Tampilkan dasbor aplikasi.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $itemProducts = Product::orderBy('created_at', 'desc')->take(3)->get();
        $itemImage = ProductImage::get();
        $itemCategories = Category::where('nama', '!=', 'testimoni')->orderBy('nama', 'asc')->take(6)->get();

        return view('frontend.home', compact('itemProduct', 'itemkategori'));
    }

    public function cartInfo() {
        $user = auth()->user();
        $cartItems = Cart::where('user_id', $user->id)->get();
        $totalItems = $cartItems->unique('product_id')->count();
        return view('frontend.home', compact('totalItems'));
    }
}
