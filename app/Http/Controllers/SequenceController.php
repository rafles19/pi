<?php

namespace App\Http\Controllers;

use App\Models\About;
use Illuminate\Http\Request;

class SequenceController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $section = About::findOrFail($request->section);
        $sequence = $section->sequence->pluck(('sequence'));
        return response()->json($sequence);

    }
}
