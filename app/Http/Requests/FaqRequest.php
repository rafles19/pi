<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FaqRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'sequence' => ['required'],
            'section' => ['required'],
            'title' => ['required', 'max:255'],
            'description' => ['required'],
            'status' => ['required'],
            'image' => ['nullable', 'max:2048', 'image']
        ];
    }
}
