<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAboutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'sequence' => ['nullable'],
            'section' => ['required'],
            'image_url' => ['nullable', 'max:2048', 'image'],
            'title' => ['required', 'max:255'],
            'description' => ['required'],
            'button_link' => ['required'],
            'link_url' => ['required'],
            'status' => ['required']
        ];
    }
}
