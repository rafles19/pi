<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return True;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'image' => ['required', 'max:2048', 'image'],
            'title' => ['required', 'max:255', Rule::unique('articles', 'title')],
            'category_id' => ['required', 'integer'],
            'description' => ['required'],
            'slug' => ['nullable'],
            'keywords' => ['required'],
            'published_date' => ['required'],
            'status' => ['required'],
            'lead' => ['required']
        ];
    }
}
