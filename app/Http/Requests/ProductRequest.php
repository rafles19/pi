<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'category_id' => ['required', 'integer'],
            'slug' => ['required'],
            'price' => ['required'],
            'images' => 'required|array',
            'name' => ['required', Rule::unique('products', 'name')],
            'weight' => ['required'],
            'stock' => ['required'],
            'description' => ['required'],
            'status' => ['required'],
        ];
    }
}
