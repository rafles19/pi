<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TestimonialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'description' => ['required'],
            'slug' => ['nullable'],
            'impression' => ['required'],
            'published_date' => ['required'],
            'status' => ['required'],
            'stay_for' => ['nullable'],
            'bulan' => ['required'],
            'jumlah_malam' => ['required']

        ];
    }
}
