<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->foreignId('category_id')->index()->constrained();
            $table->string('title');
            $table->string('title_id')->nullable();
            $table->string('title_ch')->nullable();
            $table->string('slug');
            $table->string('slug_id')->nullable();
            $table->string('lead')->nullable();
            $table->string('lead_id')->nullable();
            $table->string('lead_ch')->nullable();
            $table->longText('description');
            $table->string('description_id')->nullable();
            $table->string('description_ch')->nullable();
            $table->string('image');
            $table->string('thumbnail_image')->nullable();
            $table->string('impression')->nullable();
            $table->string('impression_id')->nullable();
            $table->string('impression_ch')->nullable();
            $table->string('keywords')->nullable();
            $table->string('author')->nullable();
            $table->string('stay_for')->nullable();
            $table->string('stay_for_id')->nullable();
            $table->string('stay_for_ch')->nullable();
            $table->string('views')->default(0);
            $table->date('published_date');
            $table->string('status');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('articles');
    }
};
